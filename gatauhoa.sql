-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 10, 2019 at 08:40 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gatauhoa`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_ga`
--

CREATE TABLE `t_ga` (
  `MaGa` int(20) NOT NULL,
  `TenGa` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `DiaChi` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `DienThoai` int(20) NOT NULL,
  `TruongGa` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_ga`
--

INSERT INTO `t_ga` (`MaGa`, `TenGa`, `DiaChi`, `DienThoai`, `TruongGa`, `updated_at`) VALUES
(4, 'Hà Nội', 'phường Cửa Nam, quận Hoàn Kiếm, Hà Nội', 19001000, 'Hà Nội', NULL),
(5, 'Thanh Hóa', 'Xã Hoàng Giang, huyện Nông Cống, Thanh Hóa', 19001001, 'Thanh Hóa', NULL),
(6, 'Vinh', 'phường Quán Bàu, thành phố Vinh, Nghệ An', 19001002, 'Nghệ An', '2019-11-23'),
(7, 'Huế', 'phường An Đông, thành phố Huế, Thừa Thiên - Huế', 19001003, 'Huế', NULL),
(8, 'Đà Nẵng', 'phường Tam Thuận, quận Thanh Khê, Đà Nẵng', 19001004, 'Đà Nẵng', NULL),
(9, 'Diêu Trì', 'thị trấn Diêu Trì, huyện Tuy Phước, Bình Định', 19001006, 'Bình Định', NULL),
(10, 'Nha Trang', 'phường Phước Tân, thành phố Nha Trang, Khánh Hòa', 19001007, 'Khánh Hòa', NULL),
(11, 'Sài Gòn', 'phường 9, quận 3, Thành phố Hồ Chí Minh', 19001007, 'TP Hồ Chí Minh', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_giotau`
--

CREATE TABLE `t_giotau` (
  `MaTau` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `MaGa` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `GioDen` time NOT NULL,
  `GioDi` time NOT NULL,
  `id` int(11) NOT NULL,
  `ChieuDi` int(11) DEFAULT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_giotau`
--

INSERT INTO `t_giotau` (`MaTau`, `MaGa`, `GioDen`, `GioDi`, `id`, `ChieuDi`, `updated_at`) VALUES
('5', '4', '02:30:00', '03:00:00', 1, 0, '2019-11-24'),
('5', '5', '03:30:00', '04:00:00', 2, 0, '2019-11-24'),
('5', '6', '04:30:00', '05:00:00', 3, 0, '2019-11-24'),
('5', '7', '05:30:00', '06:00:00', 4, 0, '2019-11-24'),
('5', '8', '06:30:00', '07:00:00', 6, 0, '2019-11-24'),
('5', '9', '07:30:00', '08:00:00', 7, 0, NULL),
('5', '10', '08:30:00', '09:00:00', 8, 0, NULL),
('5', '11', '09:30:00', '10:00:00', 9, 0, NULL),
('6', '4', '13:00:00', '13:30:00', 10, 0, NULL),
('6', '5', '14:00:00', '14:30:00', 11, 0, NULL),
('6', '6', '15:00:00', '15:30:00', 12, 0, NULL),
('6', '7', '16:00:00', '16:30:00', 13, 0, NULL),
('6', '8', '17:00:00', '17:30:00', 14, 0, NULL),
('6', '9', '18:00:00', '18:30:00', 15, 0, NULL),
('6', '10', '19:00:00', '19:30:00', 16, 0, NULL),
('6', '11', '20:00:00', '20:30:00', 17, 0, '2019-11-24'),
('7', '4', '10:00:00', '10:30:00', 18, 0, NULL),
('7', '5', '11:00:00', '11:30:00', 19, 0, NULL),
('7', '6', '00:00:00', '00:30:00', 20, 0, NULL),
('7', '7', '13:00:00', '13:30:00', 21, 0, '2019-11-24'),
('7', '8', '14:00:00', '14:30:00', 22, 0, '2019-11-24'),
('7', '9', '15:00:00', '15:30:00', 23, 0, '2019-11-24'),
('7', '10', '16:00:00', '16:30:00', 24, 0, '2019-11-24'),
('7', '11', '17:00:00', '17:30:00', 26, 0, '2019-11-24');

-- --------------------------------------------------------

--
-- Table structure for table `t_khachhang`
--

CREATE TABLE `t_khachhang` (
  `TenKhachHang` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `id` int(11) NOT NULL,
  `CMND` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `DienThoai` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `DiaChi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_khachhang`
--

INSERT INTO `t_khachhang` (`TenKhachHang`, `id`, `CMND`, `DienThoai`, `DiaChi`, `Email`, `updated_at`) VALUES
('Doyouevent', 1, '1111', '1234567810', '9 le van thiem', 'email@gmail.com', NULL),
('Việt', 2, '', '0346533312', 'Mỹ Đình - Hà Nội', 'buituanviet12345@gmail.com', '2019-12-08'),
('Việt', 3, '', '0346533312', 'Mỹ Đình - Hà Nội', 'buituanviet12345@gmail.com', '2019-12-08'),
('Việt', 4, '', '0346533312', 'Mỹ Đình - Hà Nội', 'buituanviet12345@gmail.com', '2019-12-08'),
('Việt', 5, '', '0346533312', 'Mỹ Đình - Hà Nội', 'buituanviet12345@gmail.com', '2019-12-08'),
('Việt', 6, '', '0346533312', 'Mỹ Đình - Hà Nội', 'buituanviet12345@gmail.com', '2019-12-08'),
('Việt', 7, '', '0346533312', 'Mỹ Đình - Hà Nội', 'buituanviet12345@gmail.com', '2019-12-08'),
('Việt', 8, '', '0346533312', 'Mỹ Đình - Hà Nội', 'buituanviet12345@gmail.com', '2019-12-08'),
('a', 9, '', '0346533312', '9 le van thiem', 'email@gmail.com', '2019-12-08'),
('a', 11, '', '0346533312', 'Mỹ Đình - Hà Nội', 'buituanviet12345@gmail.com', '2019-12-08'),
('a', 12, '', '0346533312', 'Mỹ Đình - Hà Nội', 'buituanviet12345@gmail.com', '2019-12-08'),
('a', 13, '', '0346533312', 'Mỹ Đình - Hà Nội', 'buituanviet12345@gmail.com', '2019-12-08'),
('Việt', 14, '', '0346533312', 'Mỹ Đình - Hà Nội', 'bui@gmail.com', '2019-12-08'),
('Việt', 15, '', '0346533312', 'Mỹ Đình - Hà Nội', 'bui@gmail.com', '2019-12-08'),
('Việt', 16, '', '0346533312', 'Mỹ Đình - Hà Nội', 'bui@gmail.com', '2019-12-08'),
('Việt', 17, '', '0346533312', 'Mỹ Đình - Hà Nội', 'bui@gmail.com', '2019-12-08'),
('Việt', 18, '', '0346533312', 'Mỹ Đình - Hà Nội', 'bui@gmail.com', '2019-12-08'),
('Việt', 19, '', '0346533312', 'Mỹ Đình - Hà Nội', 'bui@gmail.com', '2019-12-08'),
('a', 20, '', '0346533312', '9 le van thiem', 'vn3ctran@gmail.com', '2019-12-08'),
('a', 21, '', '0346533312', '9 le van thiem', 'vn3ctran@gmail.com', '2019-12-08'),
('a', 22, '', '0346533312', '9 le van thiem', 'vn3ctran@gmail.com', '2019-12-08'),
('a', 23, '', '0346533312', '9 le van thiem', 'vn3ctran@gmail.com', '2019-12-08'),
('Việt', 24, '', '0346533312', 'Mỹ Đình - Hà Nội', 'buituanviet1234@gmail.com', '2019-12-08'),
('Việt', 25, '', '0346533312', 'Mỹ Đình - Hà Nội', 'bui@gmail.com', '2019-12-08'),
('Việt', 26, '', '0346533312', 'Mỹ Đình - Hà Nội', 'bui@gmail.com', '2019-12-08'),
('Việt', 27, '', '0346533312', 'Mỹ Đình - Hà Nội', 'bui@gmail.com', '2019-12-08'),
('Việt', 28, '', '0346533312', 'Mỹ Đình - Hà Nội', 'bui@gmail.com', '2019-12-08');

-- --------------------------------------------------------

--
-- Table structure for table `t_khachhangthanhtoan`
--

CREATE TABLE `t_khachhangthanhtoan` (
  `id` int(11) NOT NULL,
  `NgayDi` date NOT NULL,
  `MaGaDi` int(11) NOT NULL,
  `MaGaDen` int(11) NOT NULL,
  `TenTau` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TinhTrang` int(11) NOT NULL COMMENT 'Tình trạng đã được chấp nhận hay chưa ',
  `TenKH` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Khách hàng thanh toán chưa',
  `DiaChi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SDT` int(20) DEFAULT NULL,
  `Email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SoVe` int(11) DEFAULT NULL,
  `TongTien` bigint(255) NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_khachhangthanhtoan`
--

INSERT INTO `t_khachhangthanhtoan` (`id`, `NgayDi`, `MaGaDi`, `MaGaDen`, `TenTau`, `TinhTrang`, `TenKH`, `DiaChi`, `SDT`, `Email`, `SoVe`, `TongTien`, `updated_at`) VALUES
(33, '2019-12-07', 6, 11, 'Tàu Số 001', 2, 'Việt', 'Mỹ Đình - Hà Nội', 346533312, 'buituanviet1234@gmail.com', 1, 700000, '2019-12-08'),
(34, '2019-12-07', 4, 7, 'Tàu số 003', 0, 'Việt', 'Mỹ Đình - Hà Nội', 346533312, 'bui@gmail.com', 50, 30000000, '2019-12-08'),
(35, '2019-12-07', 4, 7, 'Tàu số 003', 0, 'Việt', 'Mỹ Đình - Hà Nội', 346533312, 'bui@gmail.com', 50, 30000000, '2019-12-08'),
(36, '2019-12-07', 4, 7, 'Tàu số 003', 0, 'Việt', 'Mỹ Đình - Hà Nội', 346533312, 'bui@gmail.com', 50, 30000000, '2019-12-08'),
(37, '2019-12-07', 4, 7, 'Tàu số 003', 0, 'Việt', 'Mỹ Đình - Hà Nội', 346533312, 'bui@gmail.com', 50, 30000000, '2019-12-08');

-- --------------------------------------------------------

--
-- Table structure for table `t_tau`
--

CREATE TABLE `t_tau` (
  `MaTau` int(10) NOT NULL,
  `TenTau` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `SoToa` int(4) NOT NULL,
  `SoLuongTau` int(4) NOT NULL COMMENT 'Số lượng tàu có trong một mã tàu',
  `soVe` int(11) DEFAULT NULL,
  `GiaVe` bigint(20) DEFAULT NULL,
  `NgayDi` date DEFAULT NULL,
  `ChieuDi` int(11) DEFAULT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_tau`
--

INSERT INTO `t_tau` (`MaTau`, `TenTau`, `SoToa`, `SoLuongTau`, `soVe`, `GiaVe`, `NgayDi`, `ChieuDi`, `updated_at`) VALUES
(5, 'Tàu Số 001', 20, 1, 10007, 100000, '2019-12-07', 0, '2019-12-08'),
(6, 'Tàu Số 002', 20, 1, 10002, 150000, '2019-12-07', 0, '2019-12-08'),
(7, 'Tàu số 003', 20, 1, 9802, 200000, '2019-12-07', 0, '2019-12-08'),
(8, 'Tàu số 004', 20, 1, 10002, 300000, '2019-12-08', 0, '2019-12-08');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(254) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Null',
  `accesstoken` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(254) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` int(45) DEFAULT NULL,
  `name` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `age` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `point` int(11) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `remember_token`, `accesstoken`, `phone`, `image`, `role`, `name`, `updated_at`, `created_at`, `age`, `address`, `point`, `deleted_at`) VALUES
(1, 'vn3ctran@gmail.com', '$2y$10$NnWmu0CwTsKP/5ZLaHAa/OUcpMbwWlKYtxoQZOA4D6lreY7uE9pra', '3WcjPFTkHHag3y9FyWyMYSCbjspNctPxc2Gn69KES30Mw9dCMuQPvDog3ox2', NULL, '12345678', '/public/kcfinder-master/upload/images/73135808_2532366417007215_553648213899345920_n.jpg', 3, 'Tuanviet', '2019-11-23 10:10:00', NULL, 'nam', 'Cổ đông sơn tây', 0, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_ga`
--
ALTER TABLE `t_ga`
  ADD PRIMARY KEY (`MaGa`);

--
-- Indexes for table `t_giotau`
--
ALTER TABLE `t_giotau`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_khachhang`
--
ALTER TABLE `t_khachhang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_khachhangthanhtoan`
--
ALTER TABLE `t_khachhangthanhtoan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_tau`
--
ALTER TABLE `t_tau`
  ADD PRIMARY KEY (`MaTau`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_ga`
--
ALTER TABLE `t_ga`
  MODIFY `MaGa` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `t_giotau`
--
ALTER TABLE `t_giotau`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `t_khachhang`
--
ALTER TABLE `t_khachhang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `t_khachhangthanhtoan`
--
ALTER TABLE `t_khachhangthanhtoan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `t_tau`
--
ALTER TABLE `t_tau`
  MODIFY `MaTau` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
