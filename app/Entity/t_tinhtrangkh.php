<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class t_tinhtrangkh extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 't_tinhtrangkh';

    protected $primaryKey = 'CMND';

    protected $fillable = [
        'CMND',
        'MaToa',
        'MaLoaiTang',
        'NgayMua',
        'GioMua',
        'MaTau',
        'MaLoaiToa',
        'GaDi',
        'GaDen',
        'GheBD ',
        'GheKT',
        'NgayDi',
        'GioDi',
    ];
}