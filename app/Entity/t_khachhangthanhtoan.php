<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class t_khachhangthanhtoan extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 't_khachhangthanhtoan';

    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'NgayDi',
        'MaGaDi',
        'MaGaDen',
        'TinhTrang',
        'TenKH',
        'DiaChi',
        'SDT',
        'Email',
        'SoVe',
        'TongTien',
        'updated_at'

    ];
}