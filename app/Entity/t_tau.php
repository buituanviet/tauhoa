<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class t_tau extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 't_tau';

    protected $primaryKey = 'MaTau';

    protected $fillable = [
        'MaTau',
        'TenTau',
        'SoToa',
        'SoLuongTau ',
        'soGhe',
        'GiaVe',
        'NgayDi',
        'ChieuDi'

    ];
    public static function getAll(){
        return static::get();
    }
}