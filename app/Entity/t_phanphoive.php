<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class t_phanphoive extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 't_phanphoive';

    protected $primaryKey = 'MaChieuVe';

    protected $fillable = [
        'MaChieuVe',
        'MaTau',
        'MaToa',
        'MaLoaiTang',
        'MaLoaiToa',
        'SoLuong',
        'SoChoB',
        'SoChoE',
    ];
}