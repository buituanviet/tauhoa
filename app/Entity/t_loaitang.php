<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class t_loaitang extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 't_loaitang';

    protected $primaryKey = 'MaLoaiTang';

    protected $fillable = [
        'MaLoaiTang',
        'TenTang ',

    ];
}