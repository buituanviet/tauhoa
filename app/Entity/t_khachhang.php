<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class t_khachhang extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 't_khachhang';

    protected $primaryKey = 'TenKhachHang ';

    protected $fillable = [
        'id',
        'TenKhachHang ',
        'CMND ',
        'MatKhau ',
        'DienThoai',
        'SoNha ',
        'Phuong/Xa',
        'Quan/Huyen',
        'ThanhPho',
        'DiaChi',
        'Email'
    ];
}