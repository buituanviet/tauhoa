<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class t_chieuve extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 't_chieuve';

    protected $primaryKey = 'MaChieuVe';

    protected $fillable = [
        'MaChieuVe',
        'MaGaDi',
        'MaGaDen',
    ];

    public static function getAll(){
        return static::get();
    }
}