<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class t_ga extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 't_ga';

    protected $primaryKey = 'MaGa ';

    protected $fillable = [
        'MaGa ',
        'TenGa ',
        'DiaChi ',
        'DienThoai ',
        'TruongGa ',
    ];
    public static  function getAll(){
        return static::get();
    }
    
    public static  function getDiemDungWithID($id){
        return static::where('MaGa',$id)->first();
    }
}