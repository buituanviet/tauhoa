<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class t_chitiettau extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 't_chitiettau';

    protected $primaryKey = 'MaTau';

    protected $fillable = [
        'MaTau',
        'MaToa',
        'MaLoaiTau',
    ];
}