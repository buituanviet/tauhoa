<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class t_giave extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 't_giave';

    protected $primaryKey = 've_id ';

    protected $fillable = [
        've_id',
        'MaGaDi ',
        'MaGaDen ',
        'MaTau ',
        'GiaVe ',
    ];
}