<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class t_chieuphanphoive extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 't_chieuphanphoive';

    protected $primaryKey = 'MaChieuVe ';

    protected $fillable = [
        'MaChieuVe ',
        'TenChieuVe ',
    ];
}