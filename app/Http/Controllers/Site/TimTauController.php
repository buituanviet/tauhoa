<?php
namespace App\Http\Controllers\Site;

use App\Entity\t_giotau;
use App\Entity\t_khachhang;
use App\Entity\t_khachhangthanhtoan;
use App\Entity\t_tau;
use App\Http\Controllers\Controller;
use App\Ultility\Facebook;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Symfony\Component\HttpFoundation\Request;

class TimTauController extends Controller
{

    
    public function __construct(){
        $this->middleware(function ($request, $next) {
            return $next($request);

        });
    }

    public function find(Request $request){
        $gaDi = $request->input('MaGaDi');
        $chieuDi = $request->input('ChieuDi');
        $ngayDi = $request->input('ngayKhoiHanh');

        $gio_taus = t_giotau::join('t_ga','t_ga.MaGa','t_giotau.MaGa')
        ->join('t_tau','t_tau.MaTau','t_giotau.MaTau')
        ->where('t_ga.MaGa',$gaDi)
        ->where('t_tau.ChieuDi',$chieuDi)
        ->where('t_tau.NgayDi',$ngayDi)
        ->get();

        return view('site.train.tim_tau',compact('gio_taus','gaDi'));

    }

    public function detail($id,$gaDi){   

        $timeNow = Carbon::now();
       
        $gio_tau = t_giotau::join('t_ga','t_ga.MaGa','t_giotau.MaGa')
        ->join('t_tau','t_tau.MaTau','t_giotau.MaTau')
        ->where('t_giotau.MaGa',$gaDi)
        ->where('t_tau.MaTau',$id)
        ->first();

        return view('site.train.detail',compact('gio_tau','gaDi','timeNow'));
    }
    
    public function order(Request $request){
        
        $order = t_khachhangthanhtoan::insertGetId([
            'NgayDi' => $request->input('NgayDi'),
            'TinhTrang' => 0 ,
            'MaGaDi' => $request->input('MaGaDi'),
            'MaGaDen' => $request->input('MaGaDen'),
            'TenTau' => $request->input('TenTau'),
            'TenKH' => $request->input('TenKH'),
            'DiaChi' => $request->input('DiaChi'),
            'SDT' => $request->input('SDT'),
            'Email' => $request->input('Email'),
            'SoVe' => $request->input('SoVe'),
            'TongTien' => $request->input('TongTien'),
            'updated_at'=> new \DateTime()  
            ]);

        $khachhang = t_khachhang::insertGetId([
            'TenKhachHang' => $request->input('TenKH'),
            'DiaChi' => $request->input('DiaChi'),
            'CMND' => '',
            'DienThoai' => $request->input('SDT'),
            'Email' => $request->input('Email'),
            'updated_at'=> new \DateTime()  
            ]);

        $tau = t_tau::where('MaTau',$request->input('MaTau'))
        ->first();
        //so vé còn 
        
        $veCon = $tau->soVe;
         
        $updateTau = t_tau::where('MaTau',$request->input('MaTau'))
        ->update([
            'sove' => $veCon - $request->input('SoVe'),
        ]);
        
        return view('site.train.orderSuccess');
    }
}
