<?php
namespace App\Http\Controllers\Site;

use App\Entity\Information;
use App\Entity\Input;
use App\Entity\Language;
use App\Entity\Menu;
use App\Entity\MenuElement;

use App\Entity\TypeInput;
use App\Http\Controllers\Controller;
use App\Ultility\Facebook;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 10/19/2017
 * Time: 10:02 AM
 */
class SiteController extends Controller
{
    public $languageCurrent;
    public $colorLogo;
    public $logoSite;
    
    public function __construct(){
        $this->middleware(function ($request, $next) {

            return $next($request);
        });
    }

    private function getUrlLoginFacebook () {
        $app = new Facebook();

        $fb = new \Facebook\Facebook([
            'app_id' => $app->getAppId(),
            'app_secret' => $app->getAppSecret(),
            'default_graph_version' => $app->getDefaultGraphVersion()
        ]);

        $helper = $fb->getRedirectLoginHelper();


        $permissions = []; // optional
        $loginUrl = $helper->getLoginUrl(URL::to('/').'/cblogin', $permissions);

        return $loginUrl;
    }
}
