<?php

namespace App\Http\Controllers\Admin;

use App\Entity\t_khachhangthanhtoan;
use App\Entity\t_tau;
use App\Entity\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Request;
use Yajra\Datatables\Datatables;

class ThanhToanController extends Controller
{
   public function index(){
   
   $khachhanngs = t_khachhangthanhtoan::get();

    return view('admin.train.ql_thanh_toan.index',compact('khachhanngs'));

   }
   public function add(){
       
    return view('admin.train.ql_thanh_toan.add');

   }
   public function store(Request $request){

   }

   public function edit(Request $request,$id){

   $khachhang = t_khachhangthanhtoan::where('id', $id)
   ->first();

    return view('admin.train.ql_thanh_toan.edit', compact('khachhang'));

   }

//    public function update(Request $request, $id){


//    }


   public function destroy ($id){
      $khachhanngs = t_khachhangthanhtoan::where('id', $id)
      ->delete();
      return redirect()->back();
   }

   public function submitOrder(Request $request){
      $id = $request->input('id');
      $khachhanngs = t_khachhangthanhtoan::where('id', $id)
      ->update([
         'TinhTrang' => 1
      ]);
     
   }
   public function cancelOrder(Request $request){
      $id = $request->input('id');
      $khach = t_tau::where('TenTau','like', '%'.$request->input('TenTau').'%')
      ->first();

      $veCon = $khach->soVe;

      $khachhanng = t_khachhangthanhtoan::where('id', $id)
      ->update([
         'TinhTrang' => 2,
      ]);

      $tau = t_tau::where('TenTau','like', '%'.$request->input('TenTau').'%')
      ->update([
         'SoVe' => $veCon + $request->input('SoVe')
         ]);
     
   }

}
