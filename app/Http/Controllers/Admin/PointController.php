<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 10/16/2017
 * Time: 9:24 AM
 */

namespace App\Http\Controllers\Admin;

use App\Entity\diem_dung;
use App\Entity\Post;
use App\Entity\TypeSubPost;
use App\Entity\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Request;
use Yajra\Datatables\Datatables;

class PointController extends Controller
{
   public function index(){
       
    return view('admin.point.index');

   }
   public function add(){
       
    return view('admin.point.add');

   }
   public function store(Request $request){

    $diemModel = new diem_dung();
    $diem_dung = $diemModel->insertGetId([
        'luot_id' => $request->input('luot_id'),
        'ten_diem_dung' => $request->input('ten_diem_dung'),
        'created_at' => new \DateTime,
        'updated_at' => new \DateTime,
    ]);

    return redirect(route('list_point'))->with('success','Thêm mới điểm dừng thành công');

   }

   public function edit(Request $request,$id){

        $diem_dung = diem_dung::join('luot', 'luot.id', '=', 'diem_dung.luot_id')
        ->where('diem_dung.id',$id)
        ->select(
            'luot.id as luot_id',
            'luot.ten_luot',
            'diem_dung.*',
        )
        ->first();

    return view('admin.point.edit', compact('diem_dung'));

   }
   public function update(Request $request, $id){


    $diemModel = new diem_dung();

    $diem_dung = $diemModel->where('diem_dung.id',$id)
    ->update([
        'luot_id' => $request->input('luot_id'),
        'ten_diem_dung' => $request->input('ten_diem_dung'),
    ]);

    return redirect(route('list_point'))->with('successUpdate','Chỉnh sửa điểm dừng thành công');

   }


   public function destroy ($id){

        $diem_dung = diem_dung::where('id',$id)->delete();
        return redirect()->back();

   }

    public function anyDatatables(Request $request) {
        
        $diem_dung = diem_dung::join('luot', 'luot.id', '=', 'diem_dung.luot_id')
            ->select(
                'luot.ten_luot',
                'diem_dung.*',
            );

        return Datatables::of($diem_dung)

            ->addColumn('action', function($diem_dung) {

                $string =  '<a href="'.route('show_edit_point', ['id' => $diem_dung->id]).'">
                           <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                       </a>';
                $string .= '<a  href="'.route('delete_point_diemdung', ['id' => $diem_dung->id]).'" class="btn btn-danger btnDelete" >
                <i class="fa fa-trash" aria-hidden="true"></i>
                            </a>';
                return $string;
            })
            ->orderColumn('diem_dung.id', 'diem_dung.id desc')
            ->make(true);
    }
}
