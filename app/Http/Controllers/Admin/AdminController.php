<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 10/16/2017
 * Time: 9:24 AM
 */

namespace App\Http\Controllers\Admin;

use App\Entity\Post;
use App\Entity\TypeSubPost;
use App\Entity\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function home() {
		session_start();
        $user = Auth::user();
        if (User::isManager($user->role) || User::isEditor($user->role)) {
            $_SESSION['loginSuccessAdmin'] = $user->email;
        }
		
       
        $countUser = User::count();

        return View('admin.home.index', compact(
            'countUser'
        ));
    }
}
