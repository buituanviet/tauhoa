<?php

namespace App\Http\Controllers\Admin;

use App\Entity\t_giotau;
use App\Entity\Post;
use App\Entity\TypeSubPost;
use App\Entity\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Request;
use Yajra\Datatables\Datatables;

class GioTauController extends Controller
{
   public function index(){

      $gio_taus = t_giotau::join('t_ga','t_ga.MaGa','t_giotau.MaGa')
      ->join('t_tau','t_tau.MaTau','t_giotau.MaTau')
      ->paginate(10);
      
  
      return view('admin.train.ql_gio_tau.index', compact('gio_taus'));
  
     }
     public function add(){
         
      return view('admin.train.ql_gio_tau.add');
  
     }
     public function store(Request $request){
      $khach_hangModel = new t_giotau();
      $khach_hang = $khach_hangModel->insertGetId([

          'MaTau'=> $request->input('MaTau'),
          'MaGa'=> $request->input('MaGa'),
          'GioDen'=> $request->input('GioDen'),
          'GioDi'=> $request->input('GioDi'),
          'ChieuDi'=> $request->input('ChieuDi'),
        
          ]);
      
      return redirect(route('ql_gio_tau'));
  
     }
  
     public function edit(Request $request,$id){
          $gio_tau = t_giotau::where('id', $id)
          ->first();
          return view('admin.train.ql_gio_tau.edit',compact('gio_tau'));
  
     }

     public function update(Request $request, $id){
      $khach_hang = t_giotau::where('id', $id)
          ->update([
            'MaTau'=> $request->input('MaTau'),
            'MaGa'=> $request->input('MaGa'),
            'GioDen'=> $request->input('GioDen'),
            'GioDi'=> $request->input('GioDi'),
            'ChieuDi'=> $request->input('ChieuDi'),
              ]);
        
          return redirect(route('ql_gio_tau'));
     }    
  
  
     public function destroy ($id){
      $khach_hang = t_giotau::where('id', $id)
      ->delete();
      return redirect(route('ql_gio_tau'));
     }
}
