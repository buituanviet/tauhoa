<?php

namespace App\Http\Controllers\Admin;

use App\Entity\t_khachhang;
use App\Entity\Post;
use App\Entity\t_khach_hang;
use App\Entity\TypeSubPost;
use App\Entity\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Request;
use Yajra\Datatables\Datatables;

class KhachHangController extends Controller
{
   public function index(){

    $khach_hangs = t_khachhang::paginate(10);

    return view('admin.train.ql_khach_hang.index', compact('khach_hangs'));

   }
   public function add(){
       
    return view('admin.train.ql_khach_hang.add');

   }
   public function store(Request $request){
    $khach_hangModel = new t_khachhang();
    $khach_hang = $khach_hangModel->insertGetId([
        'TenKhachHang'=> $request->input('TenKhachHang'),
        'CMND'=> $request->input('CMND'),
        'DienThoai'=> $request->input('DienThoai'),
        'Email'=> $request->input('Email'),
        'DiaChi'=> $request->input('DiaChi'),
        ]);
    
    return redirect(route('ql_khach_hang'));

   }

   public function edit(Request $request,$id){
        $khach_hang = t_khachhang::where('id', $id)
        ->first();
        return view('admin.train.ql_khach_hang.edit',compact('khach_hang'));

   }
   public function update(Request $request, $id){
    $khach_hang = t_khachhang::where('id', $id)
        ->update([
            'TenKhachHang'=> $request->input('TenKhachHang'),
            'CMND'=> $request->input('CMND'),
            'DienThoai'=> $request->input('DienThoai'),
            'Email'=> $request->input('Email'),
            'DiaChi'=> $request->input('DiaChi'),
            ]);
        return redirect(route('ql_khach_hang'));
   }    


   public function destroy ($id){
    $khach_hang = t_khachhang::where('id', $id)
    ->delete();
    return redirect(route('ql_khach_hang'));
   }
}
