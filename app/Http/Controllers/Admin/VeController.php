<?php

namespace App\Http\Controllers\Admin;

use App\Entity\t_ga;
use App\Entity\t_tau;
use App\Entity\t_ve;

use App\Entity\Post;
use App\Entity\TypeSubPost;
use App\Entity\User;
use App\Entity\t_giave;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Request;
use Yajra\Datatables\Datatables;

class VeController extends Controller
{
  
   public function index(){

      $ves = t_giave::leftJoin('t_ga','t_ga.MaGa','t_giave.MaGaDi')
      ->leftJoin('t_tau','t_tau.MaTau','t_giave.MaTau')
      ->select(
        't_ga.TenGa as MaGaDi',
        't_ga.TenGa as MaGaDen',
        't_tau.MaTau as MaTau',
        't_giave.GiaVe',
        't_giave.ve_id'
        )
      ->get();
  
      return view('admin.train.ql_ve.index', compact('ves'));
  
     }
     public function add(){
         
      return view('admin.train.ql_ve.add');
  
     }
     public function store(Request $request){
      $khach_hangModel = new t_giave();
      $den = $request->input('MaGaDen');
      $di = $request->input('MaGaDi');
      $tau = $request->input('MaTau');
      $ve = $request->input('GiaVe');
     

      $khach_hang = $khach_hangModel->insertGetId([
            'MaGaDi'=> $di,
            'MaGaDen'=> $den,
            'MaTau'=> $tau,
            'GiaVe'=> $ve,
          ]);
      
      return redirect(route('ql_ve'));
  
     }
  
     public function edit(Request $request,$id){
          $ve = t_giave::where('ve_id', $id)
          ->first();

          return view('admin.train.ql_ve.edit',compact('ve'));
  
     }
     public function update(Request $request, $id){
      $khach_hang = t_giave::where('ve_id', $id)
          ->update([
              'MaGaDi'=> $request->input('MaGaDi'),
              'MaGaDen'=> $request->input('MaGaDen'),
              'MaTau'=> $request->input('MaTau'),
              'GiaVe'=> $request->input('GiaVe'),
              ]);
          return redirect(route('ql_ve'));
     }    
  
  
     public function destroy($id){
      $khach_hang = t_giave::where('ve_id', $id)
      ->delete();
      return redirect(route('ql_ve'));
     }
}
