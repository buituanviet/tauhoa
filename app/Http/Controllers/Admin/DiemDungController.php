<?php

namespace App\Http\Controllers\Admin;

use App\Entity\t_ga;
use App\Entity\Post;
use App\Entity\TypeSubPost;
use App\Entity\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Request;
use Yajra\Datatables\Datatables;

class DiemDungController extends Controller
{
   public function index(){

      $gas = t_ga::get();
  
      return view('admin.train.ql_ga.index', compact('gas'));
  
     }
     public function add(){
         
      return view('admin.train.ql_ga.add');
  
     }
     public function store(Request $request){
      $khach_hangModel = new t_ga();
      $khach_hang = $khach_hangModel->insertGetId([
          'TenGa'=> $request->input('TenGa'),
          'DiaChi'=> $request->input('DiaChi'),
          'DienThoai'=> $request->input('DienThoai'),
          'TruongGa'=> $request->input('TruongGa'),
          ]);
      
      return redirect(route('ql_diem_dung'));
  
     }
  
     public function edit(Request $request,$id){
          $ga = t_ga::where('MaGa', $id)
          ->first();
          return view('admin.train.ql_ga.edit',compact('ga'));
  
     }
     public function update(Request $request, $id){
      $khach_hang = t_ga::where('MaGa', $id)
          ->update([
                'TenGa'=> $request->input('TenGa'),
                'DiaChi'=> $request->input('DiaChi'),
                'DienThoai'=> $request->input('DienThoai'),
                'TruongGa'=> $request->input('TruongGa'),
              ]);
          return redirect(route('ql_diem_dung'));
     }    
  
  
     public function destroy ($id){
      $khach_hang = t_ga::where('MaGa', $id)
      ->delete();
      return redirect(route('ql_diem_dung'));
     }
}
