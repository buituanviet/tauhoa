<?php

namespace App\Http\Controllers\Admin;

use App\Entity\t_tau;
use App\Entity\Post;
use App\Entity\TypeSubPost;
use App\Entity\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Request;
use Yajra\Datatables\Datatables;

class TauController extends Controller
{
   public function index(){

      $taus = t_tau::get();
  
      return view('admin.train.ql_tau.index', compact('taus'));
  
     }
     public function add(){
         
      return view('admin.train.ql_tau.add');
  
     }
     public function store(Request $request){
      $khach_hangModel = new t_tau();
      $khach_hang = $khach_hangModel->insertGetId([
          'MaTau'=> $request->input('MaTau'),
          'TenTau'=> $request->input('TenTau'),
          'SoToa'=> $request->input('SoToa'),
          'SoLuongTau'=> $request->input('SoLuongTau'),
          'NgayDi'=> $request->input('NgayDi'),
          'ChieuDi'=> $request->input('ChieuDi'),
          'soVe'=> $request->input('soVe'),
          'GiaVe'=> $request->input('GiaVe'),
          ]);
      
      return redirect(route('ql_tau'));
  
     }
  
     public function edit(Request $request,$id){
          $tau = t_tau::where('MaTau', $id)
          ->first();
          return view('admin.train.ql_tau.edit',compact('tau'));
  
     }
     public function update(Request $request, $id){
      $khach_hang = t_tau::where('MaTau', $id)
          ->update([
            'TenTau'=> $request->input('TenTau'),
            'SoToa'=> $request->input('SoToa'),
            'SoLuongTau'=> $request->input('SoLuongTau'),
            'NgayDi'=> $request->input('NgayDi'),
            'ChieuDi'=> $request->input('ChieuDi'),
            'soVe'=> $request->input('soVe'),
            'GiaVe'=> $request->input('GiaVe'),
              ]);
          return redirect(route('ql_tau'));
     }    
  
  
     public function destroy ($id){
      $khach_hang = t_tau::where('MaTau', $id)
      ->delete();
      return redirect(route('ql_tau'));
     }
}
