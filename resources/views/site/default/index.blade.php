@extends('site.layout.site')

@section('title', isset($information['meta_title']) ? $information['meta_title'] : '')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')

        <div id="ja-containerwrap-fl">
            <div id="ja-container">
                <div class="gach5px"></div>
                <div class="form-ad">
                    <div class="form-booking">
                       <form action="{{route('tim_tau')}}" method="POST" id="action">
                       {!! csrf_field() !!}
                         {{ method_field('POST') }}
                        <h2 style="font-size:20px; text-align:center"> Tìm kiếm chuyến tàu ngay bây giờ </h2>
                        <div class="form-group">
                           <label for="input-id">Tên Điểm xuất phát</label>                           
                           <select class="form-control" name="MaGaDi" id="">      
                           @foreach( App\Entity\t_ga::getAll() as $ga)
                                    <option value="{{$ga->MaGa}}">{{$ga->TenGa}}</option>
                            @endforeach
                           </select>
                        </div>
                        <div class="form-group">
                           <label for="input-id">Chiều đi </label>                           
                           <select class="form-control" name="ChieuDi" id="">      
                                    <option value="0">Chiều Bắc Nam</option>
                                    <option value="1">Chiều Nam Bắc</option>
                           </select>
                        </div>

                        <div class="form-group">
                           <label for="input-id">Ngày khởi hành</label>
                           <input type="date" class="form-control"name="ngayKhoiHanh"  >
                        </div>
                        
                        <div class="form-group">
                         <button type="submit">
                            Tìm kiếm
                         </button>
                        </div>
                       </form>

                    </div>
                    <style>
                       button{
                          padding: 10px 20px;
                          border:1px solid #eee;
                          border-radius: 3px;
                          background: #0084ff;
                          color:#fff
                       }
                     #action{
                        padding:10px;
                     }
                     #action input,select{
                        margin:10px 0;
                     }
                       lable{
                        display: inline-block;
                        max-width: 100%;
                        margin-bottom: 5px;
                        font-weight: 700;
                       }
                       .form-group{
                        margin-bottom: 15px;
                       }
                       .form-control{
                        display: block;
                        width: 100%;
                        height: 34px;
                        padding: 6px 12px;
                        font-size: 14px;
                        line-height: 1.42857143;
                        color: #555;
                        background-color: #fff;
                        background-image: none;
                        border: 1px solid #ccc;
                        border-radius: 4px;
                        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
                        box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
                        -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
                        -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
                        -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
                        transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
                        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
                        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
                       }
                    </style>
                    <div class="form-booking">
                        <center><img src="/site/images/ve-tau-lua-gia-re.jpg" alt="vé tàu lửa trực tuyến giá rẻ" /></center>
                    </div>
                </div>
                <div class="gach5px" class="Clear"></div>

                <!--- Cam kết --->

                <div class="box-f">
                    <div class="box-icon">
                        <div class="img-icon">
                            <img src="/site/images/1.png" alt="Đảm bảo giá tốt nhất" />
                        </div>

                        <div class="box-content">
                            <h3>Đảm bảo giá tốt nhất</h3>
                            <p>Chúng tôi đảm bảo với khách hàng sẽ đặt được dịch vụ giá tốt nhất , những chương trình khuyến mại hấp dẫn nhất </p>
                        </div>
                    </div>
                    <div class="box-icon">
                        <div class="img-icon">
                            <img src="/site/images/2.png" alt="Dịch vụ tin cậy - Giá trị đích thực" />
                        </div>

                        <div class="box-content">
                            <h3>Dịch vụ tin cậy - Giá trị đích thực</h3>
                            <p> Đặt lợi ích khách hàng lên trên hết, chúng tôi hỗ trợ khách hàng nhanh và chính xác nhất với dịch vụ tin cậy, giá trị đích thực </p>
                        </div>
                    </div>
                    <div class="box-icon">
                        <div class="img-icon">
                            <img src="/site/images/3.png" alt="Đảm bảo chất lượng" />
                        </div>

                        <div class="box-content">
                            <h3>Đảm bảo chất lượng</h3>
                            <p> Chúng tôi liên kết chặt chẽ với các đối tác, khảo sát định kỳ để đảm bảo chất lượng tốt nhất của dịch vụ </p>
                        </div>
                    </div>
                </div>
                <!--- Cam kết --->

            </div>
            <div class="gach10px"></div>
            <div class="khungmainnoidungabc">
                <div class="khungkhuyenmai" style="background:#FFFFFF;">
                    <!-- -->
                    <div class="titled">
                        <h2><u>ĐẠI LÝ VÉ TÀU HỖ TRỢ 24H</u></h2></div>
                    <!-- -->
                    <div class="khungnoidungkhuyenmai">
                        <span style="color:#000000;"><span style="font-size:14px;">CTY L&Yacute; HẢI l&agrave; đại l&yacute; cung cấp dịch vụ v&eacute; t&agrave;u nhanh nhất tiện lợi nhất cho bạn mỗi ng&agrave;y, ch&uacute;ng t&ocirc;i sẽ gi&uacute;p bạn c&oacute; một dịch vụ <u><strong>đặt v&eacute; t&agrave;u</strong></u>&nbsp;nhanh ch&oacute;ng, tiết kiệm thời gian đi lại, lu&ocirc;n lu&ocirc;n hỗ trợ đến 22h tất cả c&aacute;c ng&agrave;y trong tuần.<br />
<br />
Với chức năng đặt v&eacute; xem gi&aacute; nhanh ch&oacute;ng, bạn chỉ cần chọn ga đi, ga đến, ng&agrave;y đi, ng&agrave;y về nếu c&oacute; để xem tất cả chuyến t&agrave;u v&agrave; gi&aacute; cả, chỉ cần điền đầy đủ th&ocirc;ng tin l&agrave; đặt v&eacute; th&agrave;nh c&ocirc;ng.</span></span>
                        <br />
                        <br />
                        <div style="text-align: center;">
                            <span style="color:#000000;"><span style="font-size:14px;"><img alt="vé tàu tết trực tuyến 2020" src="/site/images/ve-tau-tet-2020.jpg" style="width: 469px; height: 440px;" /></span></span>
                            <br /> &nbsp;
                        </div>
                        <span style="color:#000000;"><span style="font-size:14px;">Bạn gặp kh&oacute; khăn việc đi lại, nhều khi ho&agrave;n đổi v&eacute; t&agrave;u bất chợt đ&oacute; l&agrave; những vấn đề đại l&yacute; ch&uacute;ng t&ocirc;i sẽ hỗ trợ 24/24 để bạn c&oacute; một dịch vụ nhanh ch&oacute;ng tiện lợi khi mua v&eacute; t&agrave;u.</span></span>
                    </div>
                </div>

            </div>

            <div class="khungmainnoidungabc">
            </div>

            <div class="khungmainnoidungabc">
                <div class="khuyenmainhom">
                    <h3>
		DANH S&Aacute;CH GA T&Agrave;U TỪ NAM RA BẮC</h3>
                    <ol>
                        @foreach(App\Entity\t_ga::getAll() as $ga)
                        <li class="khuyenmaili">
                            <u>
                                <a href="#" title="Vé tàu đi Sài Gòn">
                               <strong>{{$ga->TenGa}} </strong> {{$ga->DiaChi}}</a></u></li>
                       @endforeach
                    </ol>
                </div>
            </div>
            <div class="gach10px"></div>
            <div id="ja-botnav">
                <div class="advs bannergroup" style="background:url(images/anhfooter.png);">
                    <div class="banneritem">
                        <div class="thongtinlienhe">
                            <div style="text-align: center;">
                                <br /> ĐẠI L&Yacute; B&Aacute;N <a href="ve-xe.html"><span style="color:#ffffff;">V&Eacute; </span></a>TRỰC TUYẾN 24H
                                <br /> Website: vetautructuyen.vn
                                <br /> Văn ph&ograve;ng giao dịch <a href="ve-may-bay-gia-re.html"><span style="color:#ffffff;">v&eacute;</span></a>: 65 T&acirc;n Qu&yacute;, P.T&acirc;n Qu&yacute;, T&acirc;n Ph&uacute;, TPHCM</div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
@endsection



