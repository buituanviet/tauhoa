@extends('site.layout.site')
@section('title', isset($information['meta_title']) ? $information['meta_title'] : '')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')
    <div class="content" style="padding:10px; background:#fff; display:block;height:100%;" >
        <h1 style="text-align:center">Chúc mừng bạn Đặt Vé thành công</h1>
        <p style="text-align:center">
            Chúng tôi sẽ liên hệ lại với bạn để xác nhận đơn hàng Trong thời gian  sớm nhất!
        </p>
        <p style="text-align:center">Xin cảm ơn </p>
        <a href="/">Quay về trang chủ</a>
    </div>
@endsection

