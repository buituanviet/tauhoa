@extends('site.layout.site')

@section('title', isset($information['meta_title']) ? $information['meta_title'] : '')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')

<?php
$TengaDi = App\Entity\t_ga::getDiemDungWithID($gaDi) ;

?>
<div class="content" style="padding:10px; background:#fff; display:block;height:600px;" >
    <h4 class="product-title" style="text-align:center"> <b>Điểm Dừng:</b> {{$TengaDi->TenGa}} - <b>Địa Chỉ : </b>{{$TengaDi->DiaChi}} </h3>
    <div class="details col-md-4">
    <div class="list-group">
    <h3 class="product-title"> <b>Chuyến tàu:</b> {{$gio_tau->TenTau}} </h3>
  
    <p class="product-description">
    <b> Xuất Phát Ngày :</b> {{$gio_tau->NgayDi}}
    @if($gio_tau->NgayDi < $timeNow)
    <b style="color:red"> Tàu đã chạy</b>
    @endif
    </p>
    <h4 class="price"> 
        <?php
                if($gio_tau->ChieuDi == 0 ){
                    echo "Chiều Bắc Nam";
                }
                else{                                               
                    echo "Chiều Nam Bắc";
                }
        ?>
        </h4>
 
    <h5 class="sizes"><b>Số ghế còn trống </b> : 
    @if($gio_tau->soVe <= 0)
    Tàu đã hết vé 
    @else
    {{$gio_tau->soVe}} Vé
    @endif
    </h5>
    <h5 class="colors">
    <b>Giá Vé / 1 điểm dừng</b>  : {{number_format($gio_tau->GiaVe)}} VNĐ
    </h5>

    <h5 class="colors">
    <b>Giờ Đến</b>  : {{$gio_tau->GioDen}} GMT:+7
    </h5>
    <h5 class="colors">
    <b>Giờ Đi</b>: {{$gio_tau->GioDi}} GMT:+7
    </h5>
    <img src="/site/images/train.jpg" width="100%" alt="">
    </div>
</div>

<div class="col-md-8">
    <form action="{{route('dat_hang')}}" method="POST">     
     {!! csrf_field() !!}   
        </h4>
        <div class="form-group">                  
            <lable for="exampleInputEmail1" >Chọn Ga đến </lable>  

            <input type="hidden" name="MaTau" value="{{$gio_tau->MaTau}}">
            <input type="hidden" name="TenTau" value="{{$gio_tau->TenTau}}">

            <input type="hidden" name="MaGaDi" value="{{$TengaDi->MaGa}}">
            <input type="hidden" name="NgayDi" value="{{$gio_tau->NgayDi}}">

            <select class="form-control country " name="MaGaDen" id="">
                    <option>------- Chọn điểm đến -----</option>
                    @foreach( App\Entity\t_ga::getAll() as $id => $ga)
                        <?php
                            if($gio_tau->ChieuDi == 0){
                              if($ga->MaGa > $TengaDi->MaGa){
                                 echo '<option gaId ="'.$id.'" value="'.$ga->MaGa.'">'.$ga->TenGa.'</option>';
                              }
                            }
                            else{
                              if($ga->MaGa < $TengaDi->MaGa){
                                echo '<option gaId="'.$id.'" value="'.$ga->TenGa.'">'.$ga->TenGa.'</option>';
                             }
                            }
                        ?>
                    @endforeach
            </select>
            

        </div>
        <div class="form-group">
            <lable for="exampleInputEmail1" >Tên Khách hàng </lable>  
            <input class="form-control" type="text" name="TenKH" placeholder="Tên Khách hàng" require>
        </div>
        <div class="form-group">
            <lable for="exampleInputEmail1" >Số Điện Thoại</lable>  
            <input class="form-control" type="number" name="SDT" placeholder="Số Điện Thoại" require>
        </div>  
        <div class="form-group">
            <lable for="exampleInputEmail1" >Email</lable>  
            <input class="form-control" type="text" name="Email" placeholder="Email" require>
        </div>              
        <div class="form-group">
            <lable for="exampleInputEmail1" >Địa Chỉ</lable>  
            <input class="form-control" type="text" name="DiaChi" placeholder="Địa Chỉ" require>
        </div>
        <div class="form-group">
            <lable for="exampleInputEmail1" >Số Lượng vé</lable>                  
            <input class="form-control soluong" type="number" name="SoVe" placeholder="Số Lượng vé" value="1" min="1" require>
        </div>

        <input type="hidden" name ='TongTien' class="inputTotal" value="0">
        @if($gio_tau->soVe <= 0)
        Tàu đã hết vé vui lòng đặt vào ngày khác
        @else
        <button type="submit" class="btn btn-primary">Đặt vé</button>
        @endif
        

        <span style="float:right ;color:red; font-weight:bold">Tổng tiền : <b class="total">0</b> VNĐ</span>
        
    </form>
</div>
</div>
<script>
    //Trường hợp tàu đi bắc nam
    var price = {{$gio_tau->GiaVe}};

    $(document).ready(function(){
        $("select.country").change(function(){
            var gaDen = $(this).children("option:selected").attr('gaId');
            var total = price * gaDen; 

            var giaHientai =  $(this).parent().parent().find('.inputTotal').val();
            var giaHienthi =  $(this).parent().parent().find('.total').html();

            $(this).parent().parent().find('.inputTotal').val(total);
            $(this).parent().parent().find('.total').html(total);
        });

        $('.soluong').change(function(){
            var soluong = $(this).val();
            var giaHientai =  $(this).parent().parent().find('.inputTotal').val();
            var giaHienthi =  $(this).parent().parent().find('.total').html();

            $(this).parent().parent().find('.inputTotal').val(giaHientai * soluong);
            $(this).parent().parent().find('.total').html(giaHientai * soluong);

        })
    });
 
</script>
@endsection

