@extends('site.layout.site')

@section('title', isset($information['meta_title']) ? $information['meta_title'] : '')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')

<?php
  $TengaDi = App\Entity\t_ga::getDiemDungWithID($gaDi) ;
?>

<h3 style="text-align:center">Danh sách tàu chạy qua {{$TengaDi->TenGa}}</h3>
<div class="list-group">
    @foreach($gio_taus as  $id=> $gio_tau)
    <div class="col-md-4">
        <div class="row">

            <a href="{{route('chi_tiet_tau',['id' => $gio_tau->MaTau ,'gaDi' => $TengaDi->MaGa ])}}" class="list-group-item list-group-item-action">
              
              <b>Chuyến tàu </b>: {{$gio_tau->TenTau}} 
              <br/>
              <b>Xuất Phát Ngày </b> : {{$gio_tau->NgayDi}}
              <br/>
              <b>Chiều đi  </b> :
              <?php
                  if($gio_tau->ChieuDi == 0 ){
                      echo "Chiều Bắc Nam";
                  }
                  else{                                               
                      echo "Chiều Nam Bắc";
                  }
              ?>
              <br/>
              <b>Số ghế còn trống </b> : {{$gio_tau->soVe}} Vé
              <br/>
              <b>Giờ Đến</b>  : {{$gio_tau->GioDen}} GMT:+7
              <br/>
              <b>Giờ Đi</b>: {{$gio_tau->GioDi}} GMT:+7
            </a>
        </div>  
    </div>
    @endforeach
</div>


<!-- Modal -->
@foreach($gio_taus as $id=> $gio_tau1)
<div class="modal fade" id="exampleModal{{$id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <a style="font-size:24px" class="modal-title" id="exampleModalLabel">Đặt vé Từ {{$TengaDi->TenGa}} </a>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form action="">
              <h4>Chuyến tàu <b>{{$gio_tau1->TenTau}}</b> Giờ tới điểm đến  <b>{{$gio_tau1->GioDen}}</b>  
              <?php
                if($gio_tau->ChieuDi == 0 ){
                    echo "- Chiều Bắc Nam";
                }
                else{                                               
                    echo "- Chiều Nam Bắc";
                }
              ?>
              </h4>
                <div class="form-group">                  
                  <lable for="exampleInputEmail1" >Chọn Ga đến </lable>   
                  <select class="form-control" name="MaGaDen" id="" >
                    @foreach( App\Entity\t_ga::getAll() as $ga)
                        <?php
                            if($gio_tau->ChieuDi == 0){
                              if($ga->MaGa > $TengaDi->MaGa){
                                 echo '<option value="'.$ga->MaGa.'">'.$ga->TenGa.'</option>';
                              }
                            }
                            else{
                              if($ga->MaGa < $TengaDi->MaGa){
                                echo '<option value="'.$ga->MaGa.'">'.$ga->TenGa.'</option>';
                             }
                            }
                        ?>
                    @endforeach
                  </select>
                  <input type="hidden" value="{{$TengaDi->MaGa}}" name="MaGaDi">

                </div>
                <div class="form-group">
                  <lable for="exampleInputEmail1" >Tên Khách hàng </lable>  
                  <input class="form-control" type="text" name="TenKH" placeholder="Tên Khách hàng">
                </div>
                <div class="form-group">
                  <lable for="exampleInputEmail1" >Số Điện Thoại</lable>  
                  <input class="form-control" type="text" name="SDT" placeholder="Số Điện Thoại">
                </div>              
                <div class="form-group">
                  <lable for="exampleInputEmail1" >Địa Chỉ</lable>  
                  <input class="form-control" type="text" name="DiaChi" placeholder="Địa Chỉ">
                </div>
                <div class="form-group">
                  <lable for="exampleInputEmail1" >Số Lượng vé</lable>                  
                  <input class="form-control" type="text" name="SoVe" onchange="return tinhTien(this)" placeholder="Số Lượng vé">
                </div>

            
                <button type="submit" class="btn btn-primary">Đặt vé</button>

                <span style="float-right">Tổng tiền : <b class="total"></b> </span>
             
          </form>
     
      </div>
     
    </div>
  </div>
</div>
@endforeach

@endsection
<script>
$( document ).ready(function() {
      
});
</script>
