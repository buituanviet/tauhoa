@extends('admin.layout.admin')

@section('content')
<section class="content">
    <div class="row">
        <!-- form start -->
        <form role="form" action="{{route('add_route')}}" method="POST">
        {!! csrf_field() !!}
                {{ method_field('POST') }}
            <div class="col-xs-12 col-md-12">
                <!-- Nội dung thêm mới -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Nội dung</h3>
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">
                        <div class="row">

                        <div class="form-group col-md-6">
                                <label for="exampleInputEmail1">Điểm lên tàu</label>
                                <select name="ma_diem_len" class="form-control">
                                    @foreach(App\Entity\diem_dung::getAll() as $luot)
                                         <option value="{{$luot->id}}">{{$luot->ten_diem_dung}} </option>
                                    @endforeach
                                  
                                </select>
                    
                        </div>
                        <div class="form-group col-md-6">
                                <label for="exampleInputEmail1">Điểm Xuống tàu</label>
                                <select name="ma_diem_xuong" class="form-control">
                                @foreach(App\Entity\diem_dung::getAll() as $luot)
                                         <option value="{{$luot->id}}">{{$luot->ten_diem_dung}} </option>
                                @endforeach
                                </select>
                    
                        </div>

                        
                        <div class="form-group  col-md-6" >
                            <label for="exampleInputEmail1">Tên Lượt</label>
                            <select name="luot_id" class="form-control">
                                <option value="1">Lượt đi </option>
                                <option value="2">Lượt Về </option>
                            </select>
                        </div>
                        <div class="form-group  col-md-6" >
                            <label for="exampleInputEmail1">Giá vé</label>
                            <input type="number" class="form-control" name="gia_ve" placeholder="Giá vé (VNĐ)" required="">
                        </div>


                            
                        </div>
                        <div class="form-group" style="color: red;">
                            <button type="submit" class="btn btn-primary">
                                Thêm Mới 
                            </button>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

        </div>
           
        </form>
    </div>
</section>

@endsection