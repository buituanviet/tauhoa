@extends('admin.layout.admin')

@section('content')
    <!-- Content Header (Page header) -->

    <section class="content">

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="email">
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <div class="box">
                            <div class="box-header">
                                <a  href="{{ route('show_add_point')}}"><button class="btn btn-primary">Thêm mới</button> </a>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <table id="diem_dung" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th width="5%">ID</th>
                                        <th>Tên điểm dừng </th>
                                        <th>Lượt </th>
                                        <th>Thao tác</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </div>
          
        </div>
    </section>
    @include('admin.partials.popup_delete')

@endsection


@if (session('success'))
<script>
    alert ("{{ session('success') }}");
</script>
@endif

@if (session('successUpdate'))
<script>
    alert ("{{ session('successUpdate') }}");
</script>
@endif





@push('scripts')
<script>
    $(function() {
        $('#diem_dung').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('datatable_point') !!}',
            columns: [
                { data: 'id', name: 'id' },
                { data: 'ten_diem_dung', name: 'ten_diem_dung' },
                { data: 'ten_luot', name: 'ten_luot' },
                { data: 'action', name: 'action', orderable: false, searchable: false },
            ]
        });
    });
</script>
@endpush

