@extends('admin.layout.admin')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
       Thêm Giờ Chaỵ Tàu
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Giờ Tàu</a></li>
            <li class="active">Thêm mới</li>
        </ol>
    </section>

    <section class="content box-footer">
        <div class="row">
            <!-- form start -->
            <form role="form" action="{{ route('luu_gio_tau') }}" method="POST">
                {!! csrf_field() !!}
                {{ method_field('POST') }}
                <div class="col-xs-12 col-md-12">
                        <div class="form-group" >
                            <label for="exampleInputEmail1">Tên tàu</label>
                            <select class="form-control" name="MaTau" id="">
                                @foreach( App\Entity\t_tau::getAll() as $tau)
                                    <option value="{{$tau->MaTau}}">{{$tau->TenTau}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group" >
                            <label for="exampleInputEmail1">Tên Điểm dừng</label>
                            <select class="form-control" name="MaGa" id="">
                                @foreach( App\Entity\t_ga::getAll() as $ga)
                                    <option value="{{$ga->MaGa}}">{{$ga->TenGa}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group" >   
                            <label for="exampleInputEmail1">Giờ Đến</label>
                            <input type="time" class="form-control" name="GioDen" placeholder="Giờ Đến" required="">
                        </div>
                        
                        <div class="form-group" >   
                            <label for="exampleInputEmail1">Giờ đi </label>
                            <input type="time" class="form-control" name="GioDi"  placeholder="GioDi" required="">
                        </div>
                        <div class="form-group" >   
                            <label for="exampleInputEmail1">Chiều đi </label>
                            
                            <select class="form-control" name="ChieuDi" id="">                 
                                    <option value="0">Chiều Bắc Nam</option>
                                    <option value="1">Chiều Nam Bắc</option>
                            </select>
                        </div>
                        
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Thêm mới</button>
                        </div>
               
                </div>
              
              
            </form>
        </div>
    </section>
@endsection

