@extends('admin.layout.admin')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          Tàu
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Danh sách Tàu</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <a  href="{{route('them_gio_tau')}}"><button class="btn btn-primary">Thêm mới</button> </a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="KhachHang" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>TenTau</th>
                                <th>TenGa</th>
                                <th>GioDen</th>
                                <th>GioDi</th>
                                <th>Chiều Đi</th>
                                <th>Thao tác</th>
            
                            </tr>
                           
                            </thead>
                            <tbody>
                                @foreach($gio_taus as $tau)
                                    <tr>
                                        <td>
                                        {{$tau->id}}                   
                                        </td>
                                        <td>
                                        {{$tau->TenTau}}                   
                                        </td>
                                        <td>
                                        {{$tau->TenGa}}                   
                                        </td>
                                        <td>
                                        {{$tau->GioDen}}                   
                                        </td>
                                        <td>
                                        {{$tau->GioDi }}                   
                                        </td>
                                        <td>
                                        <?php
                                            if($tau->soVe == 0 ){
                                                echo "Chiều Bắc Nam";
                                            }
                                            else{                                               
                                                echo "Chiều Nam Bắc";
                                            }
                                        ?>
                                        </td>
                                        <td>
                                        <a href="{{route('edit_gio_tau', ['id' => $tau->id])}}">
                                            <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i>
                                            </button>
                                        </a> 
                                        <a  href="{{route('xoa_gio_tau', ['id' => $tau->id])}}" class="btn btn-danger btnDelete" >
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </a>               
                                        </td>
                                </tr>   
                                @endforeach

                               
                            </tbody>
                            {{ $gio_taus->links() }}
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    @include('admin.partials.popup_delete')
    @include('admin.partials.visiable')
@endsection

