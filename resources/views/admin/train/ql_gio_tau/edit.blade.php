@extends('admin.layout.admin')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Chỉnh sửa Giờ chạy
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">tàu</a></li>
            <li class="active">Thêm mới</li>
        </ol>
    </section>

    <section class="content box-footer">
        <div class="row">
            <!-- form start -->
            <form role="form" action="{{ route('sua_gio_tau' ,['id' => $gio_tau->id]) }}" method="POST">
                {!! csrf_field() !!}
                {{ method_field('POST') }}
                <div class="col-xs-12 col-md-12">
                        <div class="form-group" >
                            <label for="exampleInputEmail1">Tên tàu</label>
                            <select class="form-control" name="MaTau" id="">
                                @foreach( App\Entity\t_tau::getAll() as $tau)
                                    <option value="{{$tau->MaTau}}" <?php if($gio_tau->MaTau == $tau->MaTau )  {echo "selected";}?> >{{$tau->TenTau}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group" >
                            <label for="exampleInputEmail1">Tên Điểm dừng</label>
                            <select class="form-control" name="MaGa" id="">
                                @foreach( App\Entity\t_ga::getAll() as $ga)
                                    <option value="{{$ga->MaGa}}" <?php if($gio_tau->MaGa == $ga->MaGa )  {echo "selected";}?>>{{$ga->TenGa}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group" >   
                            <label for="exampleInputEmail1">Giờ Đến</label>
                            <input type="time" class="form-control" name="GioDen" placeholder="Giờ Đến" value="{{$gio_tau->GioDen}}" equired="">
                        </div>
                        
                        <div class="form-group" >   
                            <label for="exampleInputEmail1">Giờ đi </label>
                            <input type="time" class="form-control" name="GioDi"  placeholder="GioDi" value="{{$gio_tau->GioDi}}" required="">
                        </div>
                        <div class="form-group" >   
                            <label for="exampleInputEmail1">Chiều đi </label>                            
                            <select class="form-control" name="ChieuDi" id="">                 
                                    <option value="0" <?php if($gio_tau->ChieuDi == 0) echo 'selected'; ?> >Chiều Bắc Nam</option>
                                    <option value="1" <?php if($gio_tau->ChieuDi == 1) echo 'selected'; ?> >Chiều Nam Bắc</option>
                            </select>
                        </div>
                        
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Thêm mới</button>
                        </div>
               
                </div>
              
              
            </form>
        </div>
    </section>
@endsection

