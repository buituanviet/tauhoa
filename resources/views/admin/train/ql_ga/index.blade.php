@extends('admin.layout.admin')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
       ga
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Danh sách ga Tàu</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <a  href="{{route('them_diem_dung')}}"><button class="btn btn-primary">Thêm mới</button> </a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="KhachHang" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>MaGa</th>
                                <th>TenGa</th>
                                <th>DiaChi</th>
                                <th>DienThoai</th>
                                <th>TruongGa</th>
                                <th>Công cụ</th>
            
                            </tr>
                           
                            </thead>
                            <tbody>
                                @foreach($gas as $ga)
                                    <tr>
                                        <td>
                                        {{$ga->MaGa}}                   
                                        </td>
                                        <td>
                                        {{$ga->TenGa}}                   
                                        </td>
                                        <td>
                                        {{$ga->DiaChi}}                   
                                        </td>
                                        <td>
                                        {{$ga->DienThoai}}                   
                                        </td>
                                        <td>
                                        {{$ga->TruongGa}}                   
                                        </td>
                                        <td>
                                        <a href="{{route('edit_diem_dung', ['id' => $ga->MaGa])}}">
                                            <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i>
                                            </button>
                                        </a> 
                                        <a  href="{{route('xoa_diem_dung', ['id' => $ga->MaGa])}}" class="btn btn-danger btnDelete" >
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </a>               
                                        </td>
                                </tr>   
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    @include('admin.partials.popup_delete')
    @include('admin.partials.visiable')
@endsection

