@extends('admin.layout.admin')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Thêm mới Ga
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Ga</a></li>
            <li class="active">Thêm mới</li>
        </ol>
    </section>

    <section class="content box-footer">
        <div class="row">
            <!-- form start -->
            <form role="form" action="{{ route('luu_diem_dung') }}" method="POST">
                {!! csrf_field() !!}
                {{ method_field('POST') }}
                <div class="col-xs-12 col-md-12">
                        <div class="form-group" >
                            <label for="exampleInputEmail1">Tên ga</label>
                            <input type="text" class="form-control" name="TenGa" placeholder="Tên ga" required="">
                        </div>
                        <div class="form-group" >
                            <label for="exampleInputEmail1">Địa Chỉ</label>
                            <input type="text" class="form-control" name="DiaChi" placeholder="Địa Chỉ" required="">
                        </div>
                        <div class="form-group" >   
                            <label for="exampleInputEmail1">Điện thoại</label>
                            <input type="number" class="form-control" name="DienThoai" placeholder="Điện thoại" required="">
                        </div>
                        <div class="form-group" >   
                            <label for="exampleInputEmail1">Trường ga</label>
                            <input type="text" class="form-control" name="TruongGa" placeholder="Trường ga" required="">
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Thêm mới</button>
                        </div>
               
                </div>
              
              
            </form>
        </div>
    </section>
@endsection

