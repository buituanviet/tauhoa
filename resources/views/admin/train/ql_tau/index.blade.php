@extends('admin.layout.admin')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          Tàu
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Danh sách Tàu</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <a  href="{{route('them_tau')}}"><button class="btn btn-primary">Thêm mới</button> </a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="KhachHang" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>MaTau</th>
                                <th>TenTau</th>
                                <th>NgayDi</th>
                                <th>SoLuongTau</th>
                                <th>Số vé Còn</th>
                                <th>Giá Vé / 1 Điểm Dừng</th>
                                <th>Chiều đi</th>
                                <th>Thao tác</th>
            
                            </tr>
                           
                            </thead>
                            <tbody>
                                @foreach($taus as $tau)
                                    <tr>
                                        <td>
                                        {{$tau->MaTau}}                   
                                        </td>
                                        <td>
                                        {{$tau->TenTau}}                   
                                        </td>
                                        <td>
                                        {{$tau->NgayDi}}                   
                                        </td>
                                        <td>
                                        {{$tau->SoLuongTau}}                   
                                        </td>
                                        <td>
                                        {{$tau->soVe}}                   
                                        </td>
                                        <td>
                                        {{number_format($tau->GiaVe)}} VNĐ                   
                                        </td>
                                        <td>
                                        <?php
                                            if($tau->ChieuDi == 0 ){
                                                echo "Chiều Bắc Nam";
                                            }
                                            else{                                               
                                                echo "Chiều Nam Bắc";
                                            }
                                        ?>
                                        </td>
                                        <td>
                                        <a href="{{route('edit_tau', ['id' => $tau->MaTau])}}">
                                            <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i>
                                            </button>
                                        </a> 
                                        <a  href="{{route('xoa_tau', ['id' => $tau->MaTau])}}" class="btn btn-danger btnDelete" >
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </a>               
                                        </td>
                                </tr>   
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    @include('admin.partials.popup_delete')
    @include('admin.partials.visiable')
@endsection

