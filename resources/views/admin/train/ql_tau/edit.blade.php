@extends('admin.layout.admin')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Chỉnh sửa tàu 
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">tàu</a></li>
            <li class="active">Thêm mới</li>
        </ol>
    </section>

    <section class="content box-footer">
        <div class="row">
            <!-- form start -->
            <form role="form" action="{{ route('sua_tau' ,['id' => $tau->MaTau]) }}" method="POST">
                {!! csrf_field() !!}
                {{ method_field('POST') }}
                <div class="col-xs-12 col-md-12">
                        <div class="form-group" >
                            <label for="exampleInputEmail1">Tên tàu</label>
                            <input type="text" class="form-control" name="TenTau" value="{{$tau->TenTau}}" placeholder="Tên tàu" required="">
                        </div>
                        <div class="form-group" >
                            <label for="exampleInputEmail1">Số Toa</label>
                            <input type="text" class="form-control" name="SoToa" value="{{$tau->SoToa}}"  placeholder="Số Toa" required="">
                        </div>
                        <div class="form-group" >   
                            <label for="exampleInputEmail1">Số lượng tàu</label>
                            <input type="text" class="form-control" name="SoLuongTau" value="{{$tau->SoLuongTau}}"  placeholder="Số lượng tàu" required="">
                        </div>
                        <div class="form-group" >   
                            <label for="exampleInputEmail1">Số lượng vé</label>
                            <input type="text" class="form-control" name="soVe" value="{{$tau->soVe}}"  placeholder="Số lượng tàu" required="">
                        </div>
                        <div class="form-group" >   
                            <label for="exampleInputEmail1">Giá vé / 1 điểm dừng</label>
                            <input type="number" class="form-control" name="GiaVe" value="{{$tau->GiaVe}}" placeholder="Giá vé" required="">
                        </div>
                        <div class="form-group" >   
                            <label for="exampleInputEmail1">Ngày Xuất phát</label>
                            <input type="date" class="form-control" name="NgayDi"  value="{{$tau->NgayDi}}" placeholder="Ngày Xuất phát" required="">
                        </div>

                        <div class="form-group" >   
                            <label for="exampleInputEmail1">Chiều đi </label>                            
                            <select class="form-control" name="ChieuDi" id="">                 
                                    <option value="0" <?php if($tau->ChieuDi == 0) echo 'selected'; ?> >Chiều Bắc Nam</option>
                                    <option value="1" <?php if($tau->ChieuDi == 1) echo 'selected'; ?> >Chiều Nam Bắc</option>
                            </select>
                        </div>
                        
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">lưu</button>
                        </div>
               
                </div>
              
              
            </form>
        </div>
    </section>
@endsection

