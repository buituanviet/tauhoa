@extends('admin.layout.admin')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Thêm mới Tàu
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Vé</a></li>
            <li class="active">Thêm mới</li>
        </ol>
    </section>

    <section class="content box-footer">
        <div class="row">
            <!-- form start -->
            <form role="form" action="{{ route('luu_tau') }}" method="POST">
                {!! csrf_field() !!}
                {{ method_field('POST') }}
                <div class="col-xs-12 col-md-12">
                        <div class="form-group" >
                            <label for="exampleInputEmail1">Tên tàu</label>
                            <input type="text" class="form-control" name="TenTau" placeholder="Tên tàu" required="">
                        </div>
                        <div class="form-group" >
                            <label for="exampleInputEmail1">Số Toa</label>
                            <input type="text" class="form-control" name="SoToa" placeholder="Số Toa" required="">
                        </div>
                        <div class="form-group" >   
                            <label for="exampleInputEmail1">Số lượng tàu</label>
                            <input type="text" class="form-control" name="SoLuongTau" placeholder="Số lượng tàu" required="">
                        </div>
                        
                        <div class="form-group" >   
                            <label for="exampleInputEmail1">Số lượng vé</label>
                            <input type="number" class="form-control" name="soVe"  placeholder="Số lượng Vé" required="">
                        </div>
                        <div class="form-group" >   
                            <label for="exampleInputEmail1">Giá vé / 1 điểm dừng</label>
                            <input type="number" class="form-control" name="GiaVe"  placeholder="Giá vé" required="">
                        </div>

                        <div class="form-group" >   
                            <label for="exampleInputEmail1">Ngày Xuất phát</label>
                            <input type="date" class="form-control" name="NgayDi"  placeholder="Ngày Xuất phát" required="">
                        </div>

                        
                        <div class="form-group" >   
                            <label for="exampleInputEmail1">Chiều đi </label>                            
                            <select class="form-control" name="ChieuDi" id="">                 
                                    <option value="0">Chiều Bắc Nam</option>
                                    <option value="1">Chiều Nam Bắc</option>
                            </select>
                        </div>

                        
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Thêm mới</button>
                        </div>
               
                </div>
              
              
            </form>
        </div>
    </section>
@endsection

