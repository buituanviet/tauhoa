@extends('admin.layout.admin')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Vé
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Danh sách Vé Tàu</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <a  href="{{route('them_ve')}}"><button class="btn btn-primary">Thêm mới</button> </a>
                    </div>
                  
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="KhachHang" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>GaDi</th>
                                <!-- <th>GaDen</th> -->
                                <th>MaTau</th>
                                <th>GiaVe</th>
                                <th>Công cụ</th>
            
                            </tr>
                           
                            </thead>
                            <tbody>
                                @foreach($ves as $ve)
                            
                                    <tr>
                                        <td>
                                        {{$ve->ve_id}}                   
                                        </td>
                                        <td>
                                        {{$ve->MaGaDi}}                   
                                        </td>
                                        <!-- <td>
                                        {{$ve->MaGaDen}}                   
                                        </td> -->
                                        <td>
                                        {{$ve->MaTau}}                   
                                        </td>
                                        <td>
                                        {{$ve->GiaVe}}                   
                                        </td>
                                        <td>
                                        <a href="{{route('edit_ve', ['id' => $ve->ve_id])}}">
                                            <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i>
                                            </button>
                                        </a> 
                                        <a  href="{{route('xoa_ve', ['id' => $ve->ve_id])}}" class="btn btn-danger btnDelete" >
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </a>               
                                        </td>
                                </tr>   
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    @include('admin.partials.popup_delete')
    @include('admin.partials.visiable')
@endsection

