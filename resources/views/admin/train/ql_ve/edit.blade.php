@extends('admin.layout.admin')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
           Chỉnh sửa vé
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">vé</a></li>
            <li class="active">sửa</li>
        </ol>
    </section>


    <section class="content">
        <div class="row">
            <!-- form start -->
            <form role="form" action="{{ route('sua_ve' , ['id' => $ve->ve_id]) }}" method="POST">
                {!! csrf_field() !!}
                {{ method_field('POST') }}
                <div class="col-xs-12 col-md-6">

                        <div class="form-group" >
                            <label for="exampleInputEmail1">Tên ga Đi</label>

                            <select class="form-control" name="MaGaDi" id="0">
                                @foreach( App\Entity\t_ga::getAll() as $gaDen)
                                    <option value="{{$gaDen->MaGa}}" <?php if($gaDen->MaGa == $ve->MaGaDi){ echo 'selected';}  ?> > {{$gaDen->TenGa}}</option>
                                @endforeach
                            </select>
                           
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tên ga Đến</label>
                            
                            <select class="form-control" name="MaGaDen" id="1">
                                @foreach(App\Entity\t_ga::getAll() as $gaDi)
                                    <option value="{{$gaDi->MaGa}}" <?php if($ve->MaGaDen == $gaDi->MaGa){ echo 'selected';}  ?>>{{$gaDi->TenGa}}</option>
                                @endforeach
                            </select>
                          
                        </div>
                        <div class="form-group" >
                            <label for="exampleInputEmail1">Chọn Tàu</label>
                            <select class="form-control" name="MaTau" id="2">
                                @foreach(App\Entity\t_tau::getAll() as $tau)
                                    <option value="{{$tau->MaTau}}" <?php if($tau->MaTau === $ve->MaTau){ echo 'selected';}  ?>>{{$tau->TenTau}}</option>
                                @endforeach
                            </select>
                           
                        </div>
                        <div class="form-group" >   
                            <label for="exampleInputEmail1">Giá Vé</label>
                            <input type="number" class="form-control" name="GiaVe" value="{{$ve->GiaVe}}"
                            placeholder="Giá vé" required="">
                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Thêm mới</button>
                        </div>
                </div>
            </form>
        </div>
    </section>
@endsection


