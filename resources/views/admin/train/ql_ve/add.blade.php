@extends('admin.layout.admin')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Thêm mới Vé
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Vé</a></li>
            <li class="active">Thêm mới</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <!-- form start -->
            <form role="form" action="{{ route('them_ve') }}" method="POST">
                {!! csrf_field() !!}
                {{ method_field('POST') }}
                <div class="col-xs-12 col-md-6">

                        <div class="form-group" >
                            <label for="exampleInputEmail1">Tên ga Đi</label>

                            <select class="form-control" name="MaGaDi" id="0">
                                @foreach( App\Entity\t_ga::getAll() as $gaDen)
                                    <option value="{{$gaDen->MaGa}}">{{$gaDen->TenGa}}</option>
                                @endforeach
                            </select>
                           
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tên ga Đến</label>
                            
                            <select class="form-control" name="MaGaDen" id="1">
                                @foreach(App\Entity\t_ga::getAll() as $gaDi)
                                    <option value="{{$gaDi->MaGa}}">{{$gaDi->TenGa}}</option>
                                @endforeach
                            </select>
                          
                        </div>
                        <div class="form-group" >
                            <label for="exampleInputEmail1">Chọn Tàu</label>
                            <select class="form-control" name="MaTau" id="2">
                                @foreach(App\Entity\t_tau::getAll() as $tau)
                                    <option value="{{$tau->MaTau}}">{{$tau->TenTau}}</option>
                                @endforeach
                            </select>
                           
                        </div>
                        <div class="form-group" >   
                            <label for="exampleInputEmail1">Giá Vé</label>
                            <input type="number" class="form-control" name="GiaVe" 
                            placeholder="Giá vé" required="">
                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Thêm mới</button>
                        </div>
                </div>
            </form>
        </div>
    </section>
@endsection

