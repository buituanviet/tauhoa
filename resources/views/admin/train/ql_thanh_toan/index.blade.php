@extends('admin.layout.admin')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
           Danh sách khách hàng đặt vé
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Danh sách Vé Tàu</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="KhachHang" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Ngày đi </th>
                                    <th>Tên KH</th>
                                    <th>Ga đi</th> 
                                    <th>Ga đến </th>
                                    <th>Tàu </th>
                                    <th>Trạng thái </th>
                                    <th>Công cụ</th>            
                                </tr>
                           
                            </thead>
                            <tbody>
                            @foreach($khachhanngs as $id => $khachhang)
                                <tr>
                                    <th>{{$id+1}}</th>
                                    <th>{{$khachhang->NgayDi}}</th>
                                    <th>{{$khachhang->TenKH}}</th>
                                    <th>
                                    <?php
                                    $gaDi = \App\Entity\t_ga::getDiemDungWithID($khachhang->MaGaDi);
                                        echo isset($gaDi->TenGa) ? $gaDi->TenGa : '' ;                                  
                                    ?>
                                    </th> 
                                    <th>
                                    <?php
                                    $gaDen = \App\Entity\t_ga::getDiemDungWithID($khachhang->MaGaDen);
                                    echo isset($gaDen->TenGa) ? $gaDen->TenGa : '' ;  
                                    ?>
                                    </th> 
                                    <th>{{$khachhang->TenTau}} </th>

                                    <th>
                                        @if($khachhang->TinhTrang == 0)
                                           <p style="color:blue">Chưa thanh toán </p> 
                                        @elseif($khachhang->TinhTrang == 1)
                                        <p style="color:green">Đã Thanh toán</p>
                                        @else
                                        <p style="color:red">Đơn hàng đã hủy</p>
                                        @endif
                                    </th>
                                    <th>
                                        <a href="{{route('edit_thanh_toan', ['id' => $khachhang->id])}}">
                                            <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i>
                                            </button>
                                        </a> 
                                        <a  href="{{route('xoa_thanh_toan', ['id' => $khachhang->id])}}" class="btn btn-danger btnDelete" >
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </a>               
                                    </th>       
                                    
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    @include('admin.partials.popup_delete')
    @include('admin.partials.visiable')
@endsection

