@extends('admin.layout.admin')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Thêm mới sản phẩm
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Bài viết</a></li>
            <li class="active">Thêm mới</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">

            <!-- form start -->
           
                {!! csrf_field() !!}
                {{ method_field('POST') }}
                <div class="col-xs-12 col-md-12">
                      <div class="box-footer">
                        <div class="col-md-4">
                        <label for="input-id">Thông tin khách hàng</label>
                        
                            <h3>Tên khách hàng : {{$khachhang->TenKH}}</h3>
                            <p>Số điện thoại : {{$khachhang->SDT}}</p>
                            <p>Email : {{$khachhang->Email}}</p>
                            <p>Địa Chỉ : {{$khachhang->DiaChi}}</p>
                        </div>
                        <div class="col-md-4">
                        <label for="input-id">Thông tin Chuyến tàu</label>
                            <h3>Tên tàu: 
                                {{$khachhang->TenTau}} </p>
                            </h3>
                            <p>Ngày xuất phát : {{$khachhang->NgayDi}}</p>
                            <p>
                                Ga đi :
                            <?php
                            $gaDi = \App\Entity\t_ga::getDiemDungWithID($khachhang->MaGaDi);
                                echo isset($gaDi->TenGa) ? $gaDi->TenGa : '' ;                                  
                            ?>
                            </p> 
                            <p>
                                Ga Đến : 
                            <?php
                            $gaDen = \App\Entity\t_ga::getDiemDungWithID($khachhang->MaGaDen);
                            echo isset($gaDen->TenGa) ? $gaDen->TenGa : '' ;  
                            ?>
                            </p>                        
                                Tình trạng đơn hàng:
                                @if($khachhang->TinhTrang == 0)
                                <b style="color:red">Chưa thanh toán </b>
                                @elseif($khachhang->TinhTrang == 1)
                                <b> Đã Thanh toán</b>
                                @else
                                <br>
                                Đơn hàng đã bị hủy ngày :<b style="color:red"> {{$khachhang->updated_at}} </b>
                                @endif
                            </p>
                            <div class="form-group">
                              <label for=""></label>
                                @if($khachhang->TinhTrang == 0)
                                <button type="submit" class="btn btn-primary" onclick="submit()">Xác nhận đơn hàng</button>
                                <button type="submit" class="btn btn-danger" onclick="cancel()">Hủy đơn hàng</button>
                                @endif
                            </div>
                        </div>
                           
                      </div>
                </div>
 
        </div>
    </section>

    <script>
        var id = {{$khachhang->id}};
        var SoVe = {{$khachhang->SoVe}};
        var TenTau = '{{$khachhang->TenTau}}';

        function submit(){
            $.ajax({
                type: "GET",
                url: "{{route('xac_nhan_don_hang')}}",
                data: {
                    id : id,
                },
                success: function (response) {
                    alert ('Xác nhận đơn hàng thành công');
                }
            });
        }

        function cancel(){
            $.ajax({
                type: "GET",
                url: "{{route('huy_don_hang')}}",
                data: {
                    id : id,
                    SoVe : SoVe,
                    TenTau : TenTau
                },
                success: function (response) {
                    alert ('Hủy đơn hàng thành công');
                }
            });
        }
    
    </script>
@endsection
