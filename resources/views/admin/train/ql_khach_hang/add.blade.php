@extends('admin.layout.admin')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Thêm mới Vé
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Vé</a></li>
            <li class="active">Thêm mới</li>
        </ol>
    </section>

    <section class="content box-footer">
        <div class="row">
            <!-- form start -->
            <form role="form" action="{{ route('luu_khach_hang') }}" method="POST">
                {!! csrf_field() !!}
                {{ method_field('POST') }}
                <div class="col-xs-12 col-md-12">
                        <div class="form-group" >
                            <label for="exampleInputEmail1">Tên khách hàng</label>
                            <input type="text" class="form-control" name="TenKhachHang" placeholder="Tên khách hàng" required="">
                        </div>
                        <div class="form-group" >
                            <label for="exampleInputEmail1">CMND</label>
                            <input type="text" class="form-control" name="CMND" placeholder="CMND" required="">
                        </div>
                        <div class="form-group" >   
                            <label for="exampleInputEmail1">Điện thoại</label>
                            <input type="text" class="form-control" name="DienThoai" placeholder="Điện thoại" required="">
                        </div>
                        <div class="form-group" >
                            <label for="exampleInputEmail1">Email</label>
                            <input type="text" class="form-control" name="Email" placeholder="Email" required="">
                        </div>
                        <div class="form-group" >
                            <label for="exampleInputEmail1">Địa chỉ</label>
                            <input type="text" class="form-control" name="DiaChi" placeholder="DiaChi" required="">
                        </div>
                  
                        <div class="">
                            <button type="submit" class="btn btn-primary">Thêm mới</button>
                        </div>
               
                </div>
              
              
            </form>
        </div>
    </section>
@endsection

