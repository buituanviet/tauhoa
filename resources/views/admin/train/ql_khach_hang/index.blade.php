@extends('admin.layout.admin')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
           Khách hàng
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Danh sách Khách hàng</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <a  href="{{route('them_khach_hang')}}"><button class="btn btn-primary">Thêm mới</button> </a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="KhachHang" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>TenKhachHang</th>
                                <th>CMND</th>
                                <th>DienThoai</th>
                                <th>Email</th>
                                <th>ThanhPho</th>
                                <th>Công cụ</th>
                            </tr>
                           
                            </thead>
                            <tbody>
                                @foreach($khach_hangs as $khach_hang)
                                    <tr>
                                        <td>
                                        {{$khach_hang->id}}                   
                                        </td>
                                        <td>
                                        {{$khach_hang->TenKhachHang}}                   
                                        </td>
                                        <td>
                                        {{$khach_hang->CMND}}                   
                                        </td>
                                        <td>
                                        {{$khach_hang->DienThoai}}                   
                                        </td>
                                        <td>
                                        {{$khach_hang->Email}}                   
                                        </td>
                                        <td>
                                        {{$khach_hang->DiaChi}}                   
                                        </td>
                                        <td>
                                        <a href="{{route('edit_khach_hang', ['id' => $khach_hang->id])}}">
                                            <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i>
                                            </button>
                                        </a> 
                                        <a  href="{{route('xoa_khach_hang', ['id' => $khach_hang->id])}}" class="btn btn-danger btnDelete" >
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </a>               
                                        </td>
                                        </tr>
                                @endforeach
                            </tbody>
                            {{ $khach_hangs->links() }}
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    @include('admin.partials.popup_delete')
    @include('admin.partials.visiable')
@endsection

