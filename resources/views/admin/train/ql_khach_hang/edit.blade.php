@extends('admin.layout.admin')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
           Chỉnh sửa Khách hàng
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#"> khách hàng</a></li>
            <li class="active">Chỉnh sửa </li>
        </ol>
    </section>

    <section class="content box-footer">
        <div class="row">
            <!-- form start -->
            <form role="form" action="{{ route('sua_khach_hang',['id' => $khach_hang->id])}}" method="POST">
                {!! csrf_field() !!}
                {{ method_field('POST') }}
                <div class="col-xs-12 col-md-12">
                        <div class="form-group" >
                            <label for="exampleInputEmail1">Tên khách hàng</label>
                            <input type="text" class="form-control" name="TenKhachHang" placeholder="Tên khách hàng" value="{{$khach_hang->TenKhachHang}}" required="">
                        </div>
                        <div class="form-group" >
                            <label for="exampleInputEmail1">CMND</label>
                            <input type="text" class="form-control" name="CMND" placeholder="CMND" value="{{$khach_hang->CMND}}" required="">
                        </div>
                        <div class="form-group" >   
                            <label for="exampleInputEmail1">Điện thoại</label>
                            <input type="text" class="form-control" name="DienThoai" placeholder="Điện thoại" value="{{$khach_hang->DienThoai}}" required="">
                        </div>
                        <div class="form-group" >
                            <label for="exampleInputEmail1">Email</label>
                            <input type="text" class="form-control" name="Email" placeholder="Email" value="{{$khach_hang->Email}}" required="">
                        </div>
                        <div class="form-group" >
                            <label for="exampleInputEmail1">Địa chỉ</label>
                            <input type="text" class="form-control" name="DiaChi" placeholder="DiaChi" value="{{$khach_hang->DiaChi}}" required="">
                        </div>
                  
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Chỉnh sửa</button>
                        </div>
               
                </div>
              
              
            </form>
        </div>
    </section>
@endsection

