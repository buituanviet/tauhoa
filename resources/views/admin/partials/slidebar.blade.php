<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <!-- <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset(Auth::user()->image) }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{Auth::user()->name}}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div> -->
        @if(!\App\Entity\User::isMember(\Illuminate\Support\Facades\Auth::user()->role))
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Menu Chính</li>
            <li class="{{ Request::is('admin/posts', 'admin/posts/create', 'admin/categories') ? 'active' : null }} treeview">
                <a href="">
                    <i class="fa fa-newspaper-o" aria-hidden="true"></i> <span>Quản lý khách hàng</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu">
                    <li class="">
                        <a href="{{route('them_khach_hang')}}"><i class="fa fa-circle-o"></i>Thêm khách hàng</a>
                    </li>
                    <li class="">
                        <a href="{{route('ql_khach_hang')}}"><i class="fa fa-circle-o"></i>Tình trạng khách hàng</a>
                    </li>
                </ul>
            </li>


            <li class="{{ Request::is('admin/posts', 'admin/posts/create', 'admin/categories') ? 'active' : null }} treeview">
                <a href="">
                    <i class="fa fa-newspaper-o" aria-hidden="true"></i> <span>Quản lý Tàu</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu">
                    <li class="">
                        <a href="{{route('them_tau')}}"><i class="fa fa-circle-o"></i>Thêm mới Tàu</a>
                    </li>
                    <li class="">
                        <a href="{{route('ql_tau')}}"><i class="fa fa-circle-o"></i>Phân phối điều chỉnh tàu</a>
                    </li>
                </ul>
            </li>

            <li class="{{ Request::is('admin/posts', 'admin/posts/create', 'admin/categories') ? 'active' : null }} treeview">
                <a href="">
                    <i class="fa fa-newspaper-o" aria-hidden="true"></i> <span>Quản lý Giờ Chạy</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu">
                    <li class="">
                        <a href="{{route('them_gio_tau')}}"><i class="fa fa-circle-o"></i>Thêm Thời gian Tàu Chạy</a>
                    </li>
                    <li class="">
                        <a href="{{route('ql_gio_tau')}}"><i class="fa fa-circle-o"></i>Phân phối điều chỉnh giờ tàu</a>
                    </li>
                </ul>
            </li>
            
            
            <li class="{{ Request::is('admin/posts', 'admin/posts/create', 'admin/categories') ? 'active' : null }} treeview">
                <a href="">
                    <i class="fa fa-newspaper-o" aria-hidden="true"></i> <span>Quản lý Điểm dừng</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu">
                    <li class="">
                        <a href="{{route('them_diem_dung')}}"><i class="fa fa-circle-o"></i>Thêm Điểm dừng</a>
                    </li>
                    <li class="">
                        <a href="{{route('ql_diem_dung')}}"><i class="fa fa-circle-o"></i>Quản lý Điểm dừng</a>
                    </li>
                </ul>
            </li>


            <li class="{{ Request::is('admin/posts', 'admin/posts/create', 'admin/categories') ? 'active' : null }} treeview">
                <a href="">
                    <i class="fa fa-newspaper-o" aria-hidden="true"></i> <span>Quản lý Giao dịch</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu">
                    <li class="">
                        <a href="{{route('ql_thanh_toan')}}"><i class="fa fa-circle-o"></i>Khách hàng đặt vé</a>
                    </li>

                    <!-- <li class="">
                        <a href=""><i class="fa fa-circle-o"></i>Khách hàng đã thành toán </a>
                    </li> -->
                </ul>
            </li>

            <!-- <li class="{{ Request::is('admin/posts', 'admin/posts/create', 'admin/categories') ? 'active' : null }} treeview">
                <a href="">
                    <i class="fa fa-newspaper-o" aria-hidden="true"></i> <span>Quản lý thanh toán</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu">
                    <li class="">
                        <a href=""><i class="fa fa-circle-o"></i>Thanh toán tiền khách hàng</a>
                    </li>
                    <li class="">
                        <a href=""><i class="fa fa-circle-o"></i>Xác nhận thanh toán </a>
                    </li>
                </ul>
            </li> -->

           
            <li class="header">Thành viên</li>
            <li class="{{ Request::is('admin/users', 'admin/users/create') ? 'active' : null }} treeview">
                <a href="{{ route('users.index') }}">
                    <i class="fa fa-wrench" aria-hidden="true"></i> <span>Quản lý thành viên</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ Request::is( 'admin/users' ) ? 'active' : null }}">
                        <a href="{{ route('users.index') }}"><i class="fa fa-circle-o"></i>Tất cả Thành viên</a>
                    </li>
                    <li class="{{ Request::is('admin/users/create') ? 'active' : null }}">
                        <a href="{{ route('users.create') }}"><i class="fa fa-circle-o"></i>Thêm mới thành viên</a>
                    </li>
                </ul>
            </li>
         



            

            

            @endif
         
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
