<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix'=>'admin','namespace'=>'Admin', 'middleware' => ['admin']],function(){
    Route::get('home', 'AdminController@home');
    Route::resource('posts', 'PostController');
    Route::get('posts-show', 'PostController@anyDatatables')->name('datatable_post');
    Route::get('posts-visiable', 'PostController@visiable')->name('visable_post');
    Route::get('posts-indexhot', 'PostController@indexHot')->name('index_hot');

    Route::resource('comments', 'CommentController');
    Route::get('comments-show', 'CommentController@anyDatatables')->name('datatable_comment');

    Route::resource('products', 'ProductController');
    Route::get('products-show', 'ProductController@anyDatatables')->name('datatable_product');

    Route::resource('contact', 'ContactController');
    Route::resource('book', 'BookController');
    Route::get('export-books', 'BookController@exportToExcel')->name('exportBooks');

    Route::resource('templates', 'TemplateController');
    Route::resource('type-information', 'TypeInformationController');
    Route::resource('type-input', 'TypeInputController');
    Route::resource('type-sub-post', 'TypeSubPostController');
    Route::resource('categories', 'CategoryController');
    Route::resource('category-products', 'CategoryProductController');
    Route::group(['prefix' => '{typePost}'], function($typePost){
        // Files
        Route::resource('sub-posts', 'SubPostController');
        Route::get('sub-posts-show', 'SubPostController@anyDatatables')->name('datatable_sub_posts');
    });
    Route::resource('information', 'InformationController', ['only' => [
        'index', 'store'
    ]]);
    Route::resource('menus', 'MenuController');
    Route::resource('users', 'UserController');
    

    Route::resource('subcribe-email', 'SubcribeEmailController');
    Route::get('subcribe-email-anyDatabase', 'SubcribeEmailController@anyDatatables')->name('subcribe-email-data');
    Route::post('group-mail/create', 'GroupMailController@store')->name('group_mail.create');
    Route::delete('group-mail/{groupMail}', 'GroupMailController@destroy')->name('group_mail.destroy');
    Route::post('send-mail', 'SubcribeEmailController@send')->name('subcribe-email_send');
    Route::get('export-email', 'SubcribeEmailController@exportToExcel')->name('exportEmail');

    Route::get('hinh-thuc-thanh-toan', 'OrderController@setting')->name('method_payment');
    Route::post('cap-nhat-ngan-hang', 'OrderController@updateBank')->name('bank');
    Route::post('cap-nhat-ma-giam-gia', 'OrderController@updateCodeSale')->name('code_sale');
    Route::post('cap-nhat-phi-ship', 'OrderController@updateShip')->name('cost_ship');
    Route::post('cap-nhat-tinh-diem', 'OrderController@updateSetting')->name('updateSetting');

    Route::get('don-hang', 'OrderController@listOrder')->name('orderAdmin');
    Route::delete('don-hang/{order}', 'OrderController@deleteOrder')->name('orderAdmin.destroy');

    Route::post('cap-nhat-trang-thai-don-hang', 'OrderController@updateStatusOrder')->name('orderUpdateStatus');

    //quản lý vé
    Route::get('quan-ly-ve', 'VeController@index')->name('ql_ve');
    Route::get('ve-show', 'VeController@anyDatatables')->name('datatable_ve');
    Route::get('add-ve', 'VeController@add')->name('them_ve');
    Route::post('add-ve', 'VeController@store')->name('luu_ve');
    Route::get('edit-ve/{id}', 'VeController@edit')->name('edit_ve');
    Route::post('update-ve/{id}', 'VeController@update')->name('sua_ve');
    Route::get('delete-ve/{id}', 'VeController@destroy')->name('xoa_ve');

    //quản lý tau
    Route::get('quan-ly-tau', 'TauController@index')->name('ql_tau');
    Route::get('tau-show', 'TauController@anyDatatables')->name('datatable_tau');
    Route::get('add-tau', 'TauController@add')->name('them_tau');
    Route::post('add-tau', 'TauController@store')->name('luu_tau');
    Route::get('edit-tau/{id}', 'TauController@edit')->name('edit_tau');
    Route::post('update-tau/{id}', 'TauController@update')->name('sua_tau');
    Route::get('delete-tau/{id}', 'TauController@destroy')->name('xoa_tau');

     //quản lý tau
     Route::get('quan-ly-gio-tau', 'GioTauController@index')->name('ql_gio_tau');
     Route::get('gio-tau-show', 'GioTauController@anyDatatables')->name('datatable_gio_tau');
     Route::get('add-gio-tau', 'GioTauController@add')->name('them_gio_tau');
     Route::post('add-gio-tau', 'GioTauController@store')->name('luu_gio_tau');
     Route::get('edit-gio-tau/{id}', 'GioTauController@edit')->name('edit_gio_tau');
     Route::post('update-gio-tau/{id}', 'GioTauController@update')->name('sua_gio_tau');
     Route::get('delete-gio-tau/{id}', 'GioTauController@destroy')->name('xoa_gio_tau');

     //quản lý diem dung
     Route::get('quan-ly-diem-dung', 'DiemDungController@index')->name('ql_diem_dung');
     Route::get('diem-dung-show', 'DiemDungController@anyDatatables')->name('datatable_diem_dung');
     Route::get('add-diem-dung', 'DiemDungController@add')->name('them_diem_dung');
     Route::post('add-diem-dung', 'DiemDungController@store')->name('luu_diem_dung');
     Route::get('edit-diem-dung/{id}', 'DiemDungController@edit')->name('edit_diem_dung');
     Route::post('update-diem-dung/{id}', 'DiemDungController@update')->name('sua_điem_dung');
     Route::get('delete-diem-dung/{id}', 'DiemDungController@destroy')->name('xoa_diem_dung');
 

    //quản lý giao dich
    Route::get('quan-ly-giao-dich', 'GiaoDichController@index')->name('ql_giao_dich');
    Route::get('giao-dich-show', 'GiaoDichController@anyDatatables')->name('datatable_giao_dich');
    Route::get('add-giao-dich', 'GiaoDichController@add')->name('them_giao_dich');
    Route::post('add-giao-dich', 'GiaoDichController@store')->name('luu_giao_dich');
    Route::get('edit-giao-dich/{id}', 'GiaoDichController@edit')->name('edit_giao_dich');
    Route::post('update-giao-dich/{id}', 'GiaoDichController@update')->name('sua_giao_dich');
    Route::get('delete-giao-dich/{id}', 'GiaoDichController@destroy')->name('xoa_giao_dich');

    //quản lý Thanh toan
    Route::get('quan-li-thanh-toan', 'ThanhToanController@index')->name('ql_thanh_toan');
    Route::get('thanh-toan-show', 'ThanhToanController@anyDatatables')->name('datatable_thanh_toan');
    Route::get('add-thanh-toan', 'ThanhToanController@add')->name('them-thanh_toan');
    Route::post('add-thanh-toan', 'ThanhToanController@store')->name('luu_thanh_toan');
    Route::get('edit-thanh-toan/{id}', 'ThanhToanController@edit')->name('edit_thanh_toan');
    Route::post('update-thanh-toan/{id}', 'ThanhToanController@update')->name('sua_thanh_toan');
    Route::get('delete-thanh-toan/{id}', 'ThanhToanController@destroy')->name('xoa_thanh_toan');

    //quản lý Thanh toan
    Route::get('quan-li-khach-hang', 'KhachHangController@index')->name('ql_khach_hang');
    Route::get('khach-hang-show', 'KhachHangController@anyDatatables')->name('datatable_khach_hang');
    Route::get('add-khach-hang', 'KhachHangController@add')->name('them_khach_hang');
    Route::post('add-khach-hang', 'KhachHangController@store')->name('luu_khach_hang');
    Route::get('edit-khach-hang/{id}', 'KhachHangController@edit')->name('edit_khach_hang');
    Route::post('update-khach-hang/{id}', 'KhachHangController@update')->name('sua_khach_hang');
    Route::get('delete-khach-hang/{id}', 'KhachHangController@destroy')->name('xoa_khach_hang');

    //ajax don hang
    Route::get('xac-nhan-don-hang', 'ThanhToanController@submitOrder')->name('xac_nhan_don_hang');
    Route::get('huy-don-hang', 'ThanhToanController@cancelOrder')->name('huy_don_hang');



});
//dang nhap
Route::group(['prefix'=>'admin', 'namespace'=>'Admin' ],function(){
    Route::get('/','LoginController@showLoginForm');
    Route::get('login','LoginController@showLoginForm')->name('login');
    Route::post('login','LoginController@login');
    Route::get('logout','LoginController@logout');
    Route::post('logout','LoginController@logout')->name('logout');
    //reset password
    Route::get('password/reset','LoginController@getReset');
    Route::post('password/reset','LoginController@postReset');
});

Route::group(['namespace'=>'Site'], function() {
    Route::get('/', 'HomeController@index')->name('home');
    Route::post('ngon-ngu', 'LanguageController@index')->name('change_language');
    //Tìm kiếm chuyến tàu 
    Route::post('/tim-tau','TimTauController@find')->name('tim_tau');
    //show chuyến tàu
    Route::get('/chi-tiet-tau/{id}/{gaDi}','TimTauController@detail')->name('chi_tiet_tau');
    //đặt hàng
    Route::post('dat-hang','TimTauController@order')->name('dat_hang');


});
Route::group(['prefix'=> '{languageCurrent}', 'namespace'=>'Site'], function( $languageCurrent) {
    Route::get('/', 'HomeController@index')->name('home');

 //   Route::get('/tim-kiem', 'ProductCategoryController@search')->name('search_product');
  //  Route::get('/tim-kiem-ajax', 'ProductCategoryController@searchAjax')->name('search_product_ajax');
 //   Route::get('rating', 'ProductController@Rating')->name('rating');
    Route::get('/tim-kiem', 'ProductCategoryController@search')->name('search');

    Route::get('/dang-ky','RegisterController@showRegistrationForm')->name('register');
    Route::post('/dang-ky','RegisterController@register');

    Route::post('/quen-mat-khau','PersonController@forgetPassword')->name('forget_password');
    Route::post('/dang-nhap','LoginController@login');
    Route::get('dang-xuat','LoginController@logout')->name('logoutHome');
    Route::post('dang-xuat','LoginController@logout');
    Route::get('cblogin','LoginController@callbackLogin');

    Route::get('login/google', 'LoginController@redirectToProvider')->name('google_login');
    Route::get('login/google/callback', 'LoginController@handleProviderCallback');

    Route::get('/thong-tin-ca-nhan','PersonController@index');
    Route::post('/thong-tin-ca-nhan','PersonController@store');
    Route::get('/doi-mat-khau','PersonController@resetPassword');
    Route::post('/doi-mat-khau','PersonController@storeResetPassword');
    Route::get('/don-hang-ca-nhan','PersonController@orderPerson');

    Route::get('/lien-he','ContactController@index')->name('lien-he');
    Route::get('/contact','ContactController@index')->name('contact');
    
    Route::post('submit/contact','ContactController@submit')->name('sub_contact');
    Route::post('submit/book','ContactController@submitBook')->name('sub_book');

//    Route::post('/binh-luan', 'CommentController@index')->name('comment');
//    Route::post('/xoa-binh-luan', 'CommentController@delete');

//    Route::get('loc-san-pham', 'ProductCategoryController@filter');

    


    /*===== đặt hàng  ===== */
    Route::post('/dat-hang', 'OrderController@addToCart')->name('addToCart');
    Route::get('/dat-hang', 'OrderController@order')->name('order');
    Route::get('/xoa-don-hang', 'OrderController@deleteItemCart')->name('deleteItemCart');

    Route::post('/gui-don-hang', 'OrderController@send')->name('send');

    /*===== subcribe email   ===== */
    Route::post('subcribe-email', 'SubcribeEmailController@index')->name('subcribe_email');

    Route::get('/xem-them-image', 'SubPostController@nextImageLibrary')->name('next_image');
    Route::get('/thu-vien-hinh-anh', 'SubPostController@nextVideoLibrary')->name('next_video');
    Route::get('/picture-library', 'SubPostController@nextVideoLibrary')->name('next_video_en');

    Route::get('sitemap', 'SitemapsController@index');
//    /*===== Thương hiệu =====*/
//        // Files
//        Route::get('/menus', 'SubPostController@logo')->name('logo');
//        Route::get('/menus/{cateLogo}', 'SubPostController@logo')->name('logo-menus');
//        Route::get('/{food}', 'SubPostController@detailMenu')->name('show-detail-food');
        
    Route::get('danh-muc/{cate_slug}', 'CategoryController@index')->name('category');
    Route::get('/menus/{cate_slug}', 'ProductCategoryController@index')->name('category_product');
    Route::get('{cate_slug}/{post_slug}', 'PostController@index')->name('post');
    Route::get('{post_slug}', 'ProductController@index')->name('product');

    Route::get('/{type_sub_post}/{sub_post_slug}', 'SubPostController@index')->name('sub_post_detail');
});
